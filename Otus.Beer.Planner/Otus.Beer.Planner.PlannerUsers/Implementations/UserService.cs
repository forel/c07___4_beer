﻿using AutoMapper;
using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Models;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using Otus.Beer.Planner.DataProvider.Transactions;
using Otus.Beer.Planner.Users.Interfaces;
using Otus.Beer.Planner.Users.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.Users.Implementations
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly ITransactionManager _transactionManager;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, ITransactionManager transactionManager, IMapper mapper)
        {
            _userRepository = userRepository;
            _transactionManager = transactionManager;
            _mapper = mapper;
        }

        public async Task<UserModel> AddAsync(EditUserModel userModel)
        {
            var user = new User();
            
            AddOrUpdate(user, userModel);
            await _userRepository.AddAsync(user);
            await _transactionManager.CommitAsync();

            return new UserModel(user);
        }

        public async Task<UserModel> UpdateAsync(EditUserModel userModel)
        {
            var user = await _userRepository.GetByIdAsync(userModel.Id.Value);

            if (user == null) throw new InvalidOperationException($"User with id '{userModel.Id} not found'");

            var password = user.Password;
            var refreshToken = user.RefreshToken;

            AddOrUpdate(user, userModel);

            user.Password = password;
            user.RefreshToken = refreshToken;

            _userRepository.Update(user);
            await _transactionManager.CommitAsync();

            return new UserModel(user);
        }

        public async Task<UserModel> UpdateAsync(EditUserFromTelegramBotModel userModel)
        {
            var user = await _userRepository.GetByIdAsync(userModel.Id);

            if (user == null) throw new InvalidOperationException($"User with id '{userModel.Id} not found'");

            AddOrUpdate(user, userModel);
            _userRepository.Update(user);
            await _transactionManager.CommitAsync();

            return new UserModel(user);
        }

        public async Task<UserModel> GetAsync(int userId)
        {
            var user = await _userRepository.GetByIdAsync(userId);

            if (user == null) return null;

            return new UserModel(user);
        }

        private static void AddOrUpdate(User userEntity, EditUserModel userModel)
        {
            userEntity.Email = userModel.Email;
            userEntity.Login = userModel.Login;
            userEntity.Password = userModel.Password;
            userEntity.Phone = userModel.Phone;
            userEntity.RefreshToken = userModel.RefreshToken;
            userEntity.TelegramClientId = userModel.TelegramClientId;
        }

        private static void AddOrUpdate(User userEntity, EditUserFromTelegramBotModel userModel)
        {
            userEntity.TelegramClientId = userModel.TelegramClientId;
        }

        public async Task<UserModel> GetByTelegramClientIdAsync(string telegramClientId)
        {
            var user = await _userRepository.GetByTelegramClientIdAsync(telegramClientId);

            if (user == null) return null;

            return new UserModel(user);
        }

        public async Task<UserModel> GetByLoginAsync(string login)
        {
            var user = await _userRepository.GetByLoginAsync(login);

            if (user == null) return null;

            return new UserModel(user);
        }

        public async Task<List<UserModel>> GetUsersByFilterAsync(UserFilterModel filter)
        {
            var userFilter = _mapper.Map<UserFilter>(filter);
            return (await _userRepository.GetUsersByFilterAsync(userFilter)).Select(t => new UserModel(t)).ToList();
        }
    }
}
