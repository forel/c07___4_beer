﻿using AutoMapper;
using Otus.Beer.Planner.Users.Models;
using Otus.Beer.Planner.DataProvider.Models;

namespace Otus.Beer.Planner.Users.Mapper
{
    /// <summary>
    /// UserFilter mapper
    /// </summary>
    public class UserFilterProfile : Profile
    {
        public UserFilterProfile()
        {
            CreateMap<UserFilterModel, UserFilter>();
        }
    }
}
