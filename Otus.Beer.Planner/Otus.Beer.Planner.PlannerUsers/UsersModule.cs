﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Beer.Planner.Users.Interfaces;
using Otus.Beer.Planner.Users.Implementations;

namespace Otus.Beer.Planner.Users
{
    public static class UsersModule
    {
        public static IServiceCollection AddUsersModule(this IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
            services.AddAutoMapper(typeof(UsersModule));
            
            return services;
        }
    }
}
