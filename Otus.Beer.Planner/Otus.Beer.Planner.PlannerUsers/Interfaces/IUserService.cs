﻿using Otus.Beer.Planner.Users.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.Users.Interfaces
{
    public interface IUserService
    {
        Task<UserModel> AddAsync(EditUserModel user);
        Task<UserModel> UpdateAsync(EditUserModel user);
        Task<UserModel> UpdateAsync(EditUserFromTelegramBotModel user);
        Task<UserModel> GetAsync(int userId);
        Task<UserModel> GetByTelegramClientIdAsync(string telegramClientId);
        Task<UserModel> GetByLoginAsync(string login);
        Task<List<UserModel>> GetUsersByFilterAsync(UserFilterModel filter);
    }
}
