﻿namespace Otus.Beer.Planner.Users.Models
{
    public class EditUserFromTelegramBotModel
    {
        public int Id { get; set; }
        public string TelegramClientId { get; set; }
    }
}
