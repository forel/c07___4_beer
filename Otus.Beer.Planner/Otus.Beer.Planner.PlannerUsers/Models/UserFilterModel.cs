﻿namespace Otus.Beer.Planner.Users.Models
{
    public class UserFilterModel
    {
        public int? Id { get; set; }

        public int SearcherId { get; set; }
    }
}
