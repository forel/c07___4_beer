﻿using Otus.Beer.Planner.DataProvider.Entities;

namespace Otus.Beer.Planner.Users.Models
{
    public class UserModel
    {
        public UserModel(User entity)
        {
            Id = entity.Id;
            Login = entity.Login;
            Password = entity.Password;
            Email = entity.Email;
            Phone = entity.Phone;
            TelegramClientId = entity.TelegramClientId;
            RefreshToken = entity.RefreshToken;
        }

        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string TelegramClientId { get; set; }
        public string RefreshToken { get; set; }
    }
}
