﻿using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using Otus.Beer.Planner.DataProvider.Transactions;
using Otus.Beer.Planner.Notifications.Interfaces;
using Otus.Beer.Planner.Notifications.Models;
using System;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.Notifications.Implementations
{
    public class NotificationService : INotificationService
    {
        private readonly INotificationRepository _notificationRepository;
        private readonly ITransactionManager _transactionManager;

        public NotificationService(INotificationRepository notificationRepository, ITransactionManager transactionManager)
        {
            _notificationRepository = notificationRepository;
            _transactionManager = transactionManager;
        }

        public async Task<NotificationModel> AddAsync(EditNotificationModel notificationModel)
        {
            var notification = new Notification();

            AddOrUpdate(notification, notificationModel);
            await _notificationRepository.AddAsync(notification);
            await _transactionManager.CommitAsync();

            return new NotificationModel(notification);
        }

        public async Task<NotificationModel> UpdateAsync(EditNotificationModel notificationModel)
        {
            var notification = await _notificationRepository.GetByIdAsync(notificationModel.Id.Value);

            if (notification == null) throw new InvalidOperationException($"Notification with id '{notificationModel.Id} not found'");

            AddOrUpdate(notification, notificationModel);
            _notificationRepository.Update(notification);
            await _transactionManager.CommitAsync();

            return new NotificationModel(notification);
        }

        public async Task<NotificationModel> GetAsync(int notificationId)
        {
            var notification = await _notificationRepository.GetByIdAsync(notificationId);

            if (notification == null) return null;

            return new NotificationModel(notification);
        }

        private static void AddOrUpdate(Notification notificationEntity, EditNotificationModel notificationModel)
        {
            notificationEntity.ChannelId = notificationModel.ChannelId;
            notificationEntity.Content = notificationModel.Content;
            notificationEntity.Title = notificationModel.Title;
        }
    }
}
