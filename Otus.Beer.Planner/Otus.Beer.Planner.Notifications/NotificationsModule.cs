﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Beer.Planner.Notifications.Implementations;
using Otus.Beer.Planner.Notifications.Interfaces;

namespace Otus.Beer.Planner.Notifications
{
    public static class NotificationsModule
    {
        public static IServiceCollection AddNotificationsModule(this IServiceCollection services)
        {
            services.AddScoped<INotificationService, NotificationService>();

            return services;
        }
    }
}
