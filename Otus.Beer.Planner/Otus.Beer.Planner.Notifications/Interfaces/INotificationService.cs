﻿using Otus.Beer.Planner.Notifications.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.Notifications.Interfaces
{
    public interface INotificationService
    {
        Task<NotificationModel> AddAsync(EditNotificationModel notification);
        Task<NotificationModel> UpdateAsync(EditNotificationModel notification);
        Task<NotificationModel> GetAsync(int notificationId);
    }
}
