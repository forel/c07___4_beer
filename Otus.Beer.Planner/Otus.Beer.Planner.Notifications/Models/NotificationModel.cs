﻿using Otus.Beer.Planner.DataProvider.Entities;

namespace Otus.Beer.Planner.Notifications.Models
{
    public class NotificationModel
    {
        public NotificationModel(Notification entity)
        {
            Id = entity.Id;
            Title = entity.Title;
            Content = entity.Content;
            ChannelId = entity.ChannelId;
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int ChannelId { get; set; }
    }
}
