﻿namespace Otus.Beer.Planner.Notifications.Models
{
    public class EditNotificationModel
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int ChannelId { get; set; }
    }
}
