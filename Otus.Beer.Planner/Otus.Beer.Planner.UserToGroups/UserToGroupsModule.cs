﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Beer.Planner.UserToGroups.Implementations;
using Otus.Beer.Planner.UserToGroups.Interfaces;

namespace Otus.Beer.Planner.UserToGroups
{
    public static class UserToGroupsModule
    {
        public static IServiceCollection AddUserToGroupsModule(this IServiceCollection services)
        {
            services.AddScoped<IUserToGroupService, UserToGroupService>();

            return services;
        }
    }
}
