﻿using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using Otus.Beer.Planner.DataProvider.Transactions;
using Otus.Beer.Planner.UserToGroups.Interfaces;
using Otus.Beer.Planner.UserToGroups.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.UserToGroups.Implementations
{
    public class UserToGroupService : IUserToGroupService
    {
        private readonly IUserToGroupRepository _userToGroupRepository;
        private readonly ITransactionManager _transactionManager;

        public UserToGroupService(IUserToGroupRepository userToGroupRepository, ITransactionManager transactionManager)
        {
            _userToGroupRepository = userToGroupRepository;
            _transactionManager = transactionManager;
        }

        public async Task<UserToGroupModel> AddAsync(EditUserToGroupModel userToGroupModel)
        {
            var userToGroup = new UserToGroup();

            AddOrUpdate(userToGroup, userToGroupModel);
            await _userToGroupRepository.AddAsync(userToGroup);
            await _transactionManager.CommitAsync();

            return new UserToGroupModel(userToGroup);
        }

        public async Task<UserToGroupModel> UpdateAsync(EditUserToGroupModel userToGroupModel)
        {
            var userToGroup = await _userToGroupRepository.GetByIdAsync(userToGroupModel.Id.Value);

            if (userToGroup == null) throw new InvalidOperationException($"UserToGroup with id '{userToGroupModel.Id} not found'");

            AddOrUpdate(userToGroup, userToGroupModel);
            _userToGroupRepository.Update(userToGroup);
            await _transactionManager.CommitAsync();

            return new UserToGroupModel(userToGroup);
        }

        public async Task<UserToGroupModel> GetAsync(int userToGroupId)
        {
            var userToGroup = await _userToGroupRepository.GetByIdAsync(userToGroupId);

            if (userToGroup == null) return null;

            return new UserToGroupModel(userToGroup);
        }

        private static void AddOrUpdate(UserToGroup userToGroupEntity, EditUserToGroupModel userToGroupModel)
        {
            userToGroupEntity.UserId = userToGroupModel.UserId;
            userToGroupEntity.UserGroupId = userToGroupModel.UserGroupId;
        }

        public async Task<List<UserToGroupModel>> GetAllByUserGroupIdAsync(int userGroupId)
        {
            return (await _userToGroupRepository.GetAllByUserGroupIdAsync(userGroupId)).Select(s => new UserToGroupModel(s)).ToList();
        }

        public async Task<List<UserToGroupModel>> GetAllByUserIdAsync(int userId)
        {
            return (await _userToGroupRepository.GetAllByUserIdAsync(userId)).Select(s => new UserToGroupModel(s)).ToList();
        }
    }
}
