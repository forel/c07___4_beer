﻿using Otus.Beer.Planner.DataProvider.Entities;

namespace Otus.Beer.Planner.UserToGroups.Models
{
    public class UserToGroupModel
    {
        public UserToGroupModel(UserToGroup entity)
        {
            Id = entity.Id;
            UserId = entity.UserId;
            UserGroupId = entity.UserGroupId;
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public int UserGroupId { get; set; }
    }
}
