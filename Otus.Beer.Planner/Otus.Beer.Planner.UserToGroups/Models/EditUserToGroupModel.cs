﻿namespace Otus.Beer.Planner.UserToGroups.Models
{
    public class EditUserToGroupModel
    {
        public int? Id { get; set; }
        public int UserId { get; set; }
        public int UserGroupId { get; set; }
    }
}
