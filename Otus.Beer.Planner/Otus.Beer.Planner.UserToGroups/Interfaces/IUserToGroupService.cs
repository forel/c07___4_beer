﻿using Otus.Beer.Planner.UserToGroups.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.UserToGroups.Interfaces
{
    public interface IUserToGroupService
    {
        Task<UserToGroupModel> AddAsync(EditUserToGroupModel userToGroup);
        Task<UserToGroupModel> UpdateAsync(EditUserToGroupModel userToGroup);
        Task<UserToGroupModel> GetAsync(int userToGroupId);
        Task<List<UserToGroupModel>> GetAllByUserGroupIdAsync(int userGroupId);
        Task<List<UserToGroupModel>> GetAllByUserIdAsync(int userId);
    }
}
