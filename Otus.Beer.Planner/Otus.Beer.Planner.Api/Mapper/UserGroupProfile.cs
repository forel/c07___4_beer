﻿using AutoMapper;
using Otus.Beer.Planner.DTO.UserGroups;
using Otus.Beer.Planner.UserGroups.Models;

namespace Otus.Beer.Planner.Api.Mapper
{
    /// <summary>
    /// UserGroup mapper
    /// </summary>
    public class UserGroupProfile : Profile
    {
        public UserGroupProfile()
        {
            CreateMap<UserGroupModel, UserGroupDto>();
        }
    }
}
