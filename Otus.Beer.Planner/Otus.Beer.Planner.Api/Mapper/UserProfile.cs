﻿using AutoMapper;
using Otus.Beer.Planner.DTO.Users;
using Otus.Beer.Planner.Users.Models;

namespace Otus.Beer.Planner.Api.Mapper
{
    /// <summary>
    /// User mapper
    /// </summary>
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<EditUserDto, EditUserModel>();
            CreateMap<UserModel, UserDto>();
        }
    }
}
