﻿using AutoMapper;
using Otus.Beer.Planner.DTO.PlannerTasks;
using Otus.Beer.Planner.PlannerTasks.Models;

namespace Otus.Beer.Planner.Api.Mapper
{
    /// <summary>
    /// Planner task mapper
    /// </summary>
    public class PlannerTaskProfile : Profile
    {
        public PlannerTaskProfile()
        {
            CreateMap<EditPlannerTaskDto, EditPlannerTaskModel>();
            CreateMap<PlannerTaskModel, PlannerTaskDto>();
            CreateMap<PlannerTaskFilterDto, PlannerTaskFilterModel>();
        }
    }
}
