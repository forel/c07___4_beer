﻿using AutoMapper;
using Otus.Beer.Planner.DTO.Statuses;
using Otus.Beer.Planner.Statuses.Models;

namespace Otus.Beer.Planner.Api.Mapper
{
    /// <summary>
    /// Status mapper
    /// </summary>
    public class StatusProfile : Profile
    {
        public StatusProfile()
        {
            CreateMap<StatusModel, StatusDto>();
        }
    }
}
