﻿using AutoMapper;
using Otus.Beer.Planner.Auth.Models;
using Otus.Beer.Planner.DTO.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.Api.Mapper
{
    /// <summary>
    /// Auth data mapper
    /// </summary>
    public class AuthProfile : Profile
    {
        public AuthProfile()
        {
            CreateMap<JWTModel, JWTDto>();
        }
    }
}
