﻿using AutoMapper;
using Otus.Beer.Planner.DTO.Roles;
using Otus.Beer.Planner.Roles.Models;

namespace Otus.Beer.Planner.Api.Mapper
{
    /// <summary>
    /// Role mapper
    /// </summary>
    public class RoleProfile : Profile
    {
        public RoleProfile()
        {
            CreateMap<RoleModel, RoleDto>();
        }
    }
}
