﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Otus.Beer.Planner.Api.Helpers;
using Otus.Beer.Planner.DTO.PlannerTasks;
using Otus.Beer.Planner.Notifications.Interfaces;
using Otus.Beer.Planner.Notifications.Models;
using Otus.Beer.Planner.NotificationToUsers.Interfaces;
using Otus.Beer.Planner.NotificationToUsers.Models;
using Otus.Beer.Planner.PlannerTasks.Interfaces;
using Otus.Beer.Planner.PlannerTasks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.Api.Controllers
{
    [Route("api/plannerTasks")]
    [Authorize]
    [ApiController]
    public class PlannerTaskController : ControllerBase
    {
        private readonly IPlannerTaskService _plannerTaskService;
        private readonly INotificationService _notificationService;
        private readonly INotificationToUserService _notificationToUserService;
        private readonly IMapper _mapper;

        public PlannerTaskController(IPlannerTaskService plannerTaskService, IMapper mapper, INotificationService notificationService, INotificationToUserService notificationToUserService)
        {
            _plannerTaskService = plannerTaskService;
            _mapper = mapper;
            _notificationService = notificationService;
            _notificationToUserService = notificationToUserService;
        }

        [HttpGet("{id}")]
        public async Task<PlannerTaskDto> Get(int id)
        {
            int userId = AuthHelper.GetUserId(User);

            var taskPlannerFilterModel = new PlannerTaskFilterModel()
            {
                Id = id,
                SearcherId = userId
            };

            var tasks = await _plannerTaskService.GetTasksByFilterAsync(taskPlannerFilterModel);
            return tasks.Select(s => _mapper.Map<PlannerTaskDto>(s)).FirstOrDefault();

            /*
            var task = await _plannerTaskService.GetAsync(id);
            return _mapper.Map<PlannerTaskDto>(task);
            */
        }

        [HttpGet("search")]
        public async Task<List<PlannerTaskDto>> Search([FromQuery] PlannerTaskFilterDto filter)
        {
            int userId = AuthHelper.GetUserId(User);

            var taskPlannerFilterModel = _mapper.Map<PlannerTaskFilterModel>(filter);
            taskPlannerFilterModel.SearcherId = userId;

            var tasks = await _plannerTaskService.GetTasksByFilterAsync(taskPlannerFilterModel);
            return tasks.Select(s => _mapper.Map<PlannerTaskDto>(s)).OrderByDescending(s => s.EndDate).ToList();
        }

        [HttpPost]
        public async Task<PlannerTaskDto> Add([FromBody] EditPlannerTaskDto dto)
        {
            var taskModel = _mapper.Map<EditPlannerTaskModel>(dto);
            var task = await _plannerTaskService.AddAsync(taskModel);

            var notification = await _notificationService.AddAsync(new EditNotificationModel()
            {
                ChannelId = 1,
                Content = taskModel.Description.Substring(0, Math.Min(200, taskModel.Description.Length)),
                Title = taskModel.Title.Substring(0, Math.Min(50, taskModel.Title.Length))
            });

            await _notificationToUserService.AddAsync(new EditNotificationToUserModel()
            {
                NotificationId = notification.Id,
                UserId = task.UserId
            });

            return _mapper.Map<PlannerTaskDto>(task);
        }

        [HttpPut("{id}")]
        public async Task<PlannerTaskDto> Edit([FromBody] EditPlannerTaskDto dto)
        {
            var taskModel = _mapper.Map<EditPlannerTaskModel>(dto);
            var task = await _plannerTaskService.UpdateAsync(taskModel);
            return _mapper.Map<PlannerTaskDto>(task);
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _plannerTaskService.DeleteAsync(id);
        }
    }
}
