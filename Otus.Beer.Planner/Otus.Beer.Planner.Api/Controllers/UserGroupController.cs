﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Otus.Beer.Planner.DTO.UserGroups;
using Otus.Beer.Planner.UserGroups.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.Api.Controllers
{
    [Route("api/usergroups")]
    [Authorize]
    [ApiController]
    public class UserGroupController : ControllerBase
    {
        private readonly IUserGroupService _userGroupService;
        private readonly IMapper _mapper;

        public UserGroupController(IUserGroupService userGroupService, IMapper mapper)
        {
            _userGroupService = userGroupService;
            _mapper = mapper;
        }

        [HttpGet("getall")]
        public async Task<List<UserGroupDto>> GetAll()
        {
            var userGroups = await _userGroupService.GetAllAsync();

            return userGroups.Select(s => _mapper.Map<UserGroupDto>(s)).ToList();
        }
    }
}
