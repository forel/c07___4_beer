﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Otus.Beer.Planner.Api.Helpers;
using Otus.Beer.Planner.DTO.Users;
using Otus.Beer.Planner.UserToGroups.Interfaces;
using Otus.Beer.Planner.UserToGroups.Models;
using Otus.Beer.Planner.Users.Interfaces;
using Otus.Beer.Planner.Users.Models;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Otus.Beer.Planner.Auth.Interfaces;
using Otus.Beer.Planner.Auth.Helpers;
using Otus.Beer.Planner.UserGroups.Interfaces;
using Otus.Beer.Planner.Roles.Interfaces;
using Otus.Beer.Planner.UserToRoles.Interfaces;
using Otus.Beer.Planner.UserToRoles.Models;

namespace Otus.Beer.Planner.Api.Controllers
{
    [Route("api/users")]
    [Authorize]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly IUserToGroupService _userToGroupService;
        private readonly IPasswordGenerator _passwordGenerator;
        private readonly RegistrationNotifiationHelper _registrationNotifiationHelper;
        private readonly IUserGroupService _userGroupService;
        private readonly IRoleService _roleService;
        private readonly IUserToRoleService _userToRoleService;

        public UserController(IUserService userService,
            IMapper mapper,
            IUserToGroupService userToGroupService,
            IPasswordGenerator passwordGenerator,
            RegistrationNotifiationHelper registrationNotifiationHelper,
            IUserGroupService userGroupService,
            IRoleService roleService,
            IUserToRoleService userToRoleService)
        {
            _userService = userService;
            _mapper = mapper;
            _userToGroupService = userToGroupService;
            _passwordGenerator = passwordGenerator;
            _registrationNotifiationHelper = registrationNotifiationHelper;
            _userGroupService = userGroupService;
            _roleService = roleService;
            _userToRoleService = userToRoleService;
        }

        [HttpGet("{id}")]
        public async Task<UserDto> Get(int id)
        {
            int userId = AuthHelper.GetUserId(User);

            var userFilterModel = new UserFilterModel()
            {
                Id = id,
                SearcherId = userId
            };

            var users = await _userService.GetUsersByFilterAsync(userFilterModel);
            var user = users.FirstOrDefault();

            var userDto = _mapper.Map<UserDto>(user);

            var userGroup = await _userGroupService.GetUserGroupForUser(id);
            userDto.UserGroupId = userGroup?.Id;
            userDto.UserGroupName = userGroup?.Name;

            var role = await _roleService.GetRoleForUser(id);
            userDto.RoleId = role?.Id;
            userDto.RoleName = role?.Name;

            return userDto;
        }

        [HttpGet("getall")]
        public async Task<List<UserDto>> GetAll()
        {
            int userId = AuthHelper.GetUserId(User);

            var userFilterModel = new UserFilterModel()
            {
                SearcherId = userId
            };

            var users = await _userService.GetUsersByFilterAsync(userFilterModel);
            return users.Select(s => _mapper.Map<UserDto>(s)).ToList();
        }

        [HttpPost]
        public async Task<UserDto> Add([FromBody] EditUserDto dto)
        {
            var userModel = _mapper.Map<EditUserModel>(dto);
            userModel.Password = _passwordGenerator.Generate();

            var user = await _userService.AddAsync(userModel);

            if (dto.UserGroupId.HasValue)
            {
                var userToGroup = await _userToGroupService.AddAsync(new EditUserToGroupModel() { UserGroupId = dto.UserGroupId.Value, UserId = user.Id });
            }

            if (dto.RoleId.HasValue)
            {
                var userToRole = await _userToRoleService.AddAsync(new EditUserToRoleModel() { RoleId = dto.RoleId.Value, UserId = user.Id });
            }

            //await _registrationNotifiationHelper.Notify(user.Email, user.Password);

            return _mapper.Map<UserDto>(user);
        }

        [HttpPut("{id}")]
        public async Task<UserDto> Edit([FromBody] EditUserDto dto)
        {
            var userModel = _mapper.Map<EditUserModel>(dto);
            var user = await _userService.UpdateAsync(userModel);

            if (dto.UserGroupId.HasValue)
            {
                var userToGroup = (await _userToGroupService.GetAllByUserIdAsync(user.Id)).FirstOrDefault();

                var userToGroupEdit = await _userToGroupService.UpdateAsync(new EditUserToGroupModel() { Id = userToGroup.Id, UserGroupId = dto.UserGroupId.Value, UserId = user.Id });
            }

            if (dto.RoleId.HasValue)
            {
                var userToRole = (await _userToRoleService.GetAllByUserIdAsync(user.Id)).FirstOrDefault();

                var userToRoleEdit = await _userToRoleService.UpdateAsync(new EditUserToRoleModel() { Id = userToRole.Id, RoleId = dto.RoleId.Value, UserId = user.Id });
            }

            //await _registrationNotifiationHelper.Notify(user.Email, user.Password);

            return _mapper.Map<UserDto>(user);
        }
    }
}
