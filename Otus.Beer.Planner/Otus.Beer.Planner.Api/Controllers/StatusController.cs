﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Otus.Beer.Planner.DTO.Statuses;
using Otus.Beer.Planner.Statuses.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.Api.Controllers
{
    [Route("api/statuses")]
    [Authorize]
    [ApiController]
    public class StatusController : ControllerBase
    {
        private readonly IStatusService _statusService;
        private readonly IMapper _mapper;

        public StatusController(IStatusService statusService, IMapper mapper)
        {
            _statusService = statusService;
            _mapper = mapper;
        }

        [HttpGet("getall")]
        public async Task<List<StatusDto>> GetAll()
        {
            var statuses = await _statusService.GetAllAsync();

            return statuses.Select(s => _mapper.Map<StatusDto>(s)).ToList();
        }
    }
}
