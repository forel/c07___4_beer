﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Otus.Beer.Planner.DTO.Roles;
using Otus.Beer.Planner.Roles.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.Api.Controllers
{
    [Route("api/roles")]
    [Authorize]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _roleService;
        private readonly IMapper _mapper;

        public RoleController(IRoleService roleService, IMapper mapper)
        {
            _roleService = roleService;
            _mapper = mapper;
        }

        [HttpGet("getall")]
        public async Task<List<RoleDto>> GetAll()
        {
            var roles = await _roleService.GetAllAsync();

            return roles.Select(s => _mapper.Map<RoleDto>(s)).ToList();
        }
    }
}
