﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Beer.Planner.Auth.Interfaces;
using Otus.Beer.Planner.DTO.Auth;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.Api.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly IMapper _mapper;

        public AuthController(IAuthService authService, IMapper mapper)
        {
            _authService = authService;
            _mapper = mapper;
        }

        [HttpPost("signup")]
        public async Task SingUp(string email)
        {
            await _authService.SignUp(email);
        }

        [HttpPost("signin")]
        public async Task<JWTDto> SingIn(string login, string password)
        {
            var jwt = await _authService.SignIn(login, password);
            return _mapper.Map<JWTDto>(jwt);
        }
    }
}
