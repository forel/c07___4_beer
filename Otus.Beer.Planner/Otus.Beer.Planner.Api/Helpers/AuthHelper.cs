﻿using System.Security.Claims;

namespace Otus.Beer.Planner.Api.Helpers
{
    public class AuthHelper
    {
        public static int GetUserId(ClaimsPrincipal claimsPrincipal)
        {
            return int.Parse(claimsPrincipal.FindFirst(s => s.Type == ClaimTypes.NameIdentifier).Value);
        }
    }
}
