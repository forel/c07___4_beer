﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Beer.Planner.UserToRoles.Implementations;
using Otus.Beer.Planner.UserToRoles.Interfaces;

namespace Otus.Beer.Planner.UserToRoles
{
    public static class UserToRolesModule
    {
        public static IServiceCollection AddUserToRolesModule(this IServiceCollection services)
        {
            services.AddScoped<IUserToRoleService, UserToRoleService>();

            return services;
        }
    }
}
