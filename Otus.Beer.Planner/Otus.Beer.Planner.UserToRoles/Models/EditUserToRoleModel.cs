﻿namespace Otus.Beer.Planner.UserToRoles.Models
{
    public class EditUserToRoleModel
    {
        public int? Id { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
