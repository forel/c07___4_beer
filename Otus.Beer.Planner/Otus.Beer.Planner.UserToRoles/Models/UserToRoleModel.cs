﻿using Otus.Beer.Planner.DataProvider.Entities;

namespace Otus.Beer.Planner.UserToRoles.Models
{
    public class UserToRoleModel
    {
        public UserToRoleModel(UserToRole entity)
        {
            Id = entity.Id;
            UserId = entity.UserId;
            RoleId = entity.RoleId;
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
