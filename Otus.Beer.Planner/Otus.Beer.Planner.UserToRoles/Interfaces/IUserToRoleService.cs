﻿using Otus.Beer.Planner.UserToRoles.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.UserToRoles.Interfaces
{
    public interface IUserToRoleService
    {
        Task<UserToRoleModel> AddAsync(EditUserToRoleModel userToRole);
        Task<UserToRoleModel> UpdateAsync(EditUserToRoleModel userToRole);
        Task<UserToRoleModel> GetAsync(int userToRoleId);
        Task<List<UserToRoleModel>> GetAllByRoleIdAsync(int roleId);
        Task<List<UserToRoleModel>> GetAllByUserIdAsync(int userId);
    }
}
