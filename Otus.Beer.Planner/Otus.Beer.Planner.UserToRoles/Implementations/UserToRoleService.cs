﻿using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using Otus.Beer.Planner.DataProvider.Transactions;
using Otus.Beer.Planner.UserToRoles.Interfaces;
using Otus.Beer.Planner.UserToRoles.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.UserToRoles.Implementations
{
    public class UserToRoleService : IUserToRoleService
    {
        private readonly IUserToRoleRepository _userToRoleRepository;
        private readonly ITransactionManager _transactionManager;

        public UserToRoleService(IUserToRoleRepository userToRoleRepository, ITransactionManager transactionManager)
        {
            _userToRoleRepository = userToRoleRepository;
            _transactionManager = transactionManager;
        }

        public async Task<UserToRoleModel> AddAsync(EditUserToRoleModel userToRoleModel)
        {
            var userToRole = new UserToRole();

            AddOrUpdate(userToRole, userToRoleModel);
            await _userToRoleRepository.AddAsync(userToRole);
            await _transactionManager.CommitAsync();

            return new UserToRoleModel(userToRole);
        }

        public async Task<UserToRoleModel> UpdateAsync(EditUserToRoleModel userToRoleModel)
        {
            var userToRole = await _userToRoleRepository.GetByIdAsync(userToRoleModel.Id.Value);

            if (userToRole == null) throw new InvalidOperationException($"UserToRole with id '{userToRoleModel.Id} not found'");

            AddOrUpdate(userToRole, userToRoleModel);
            _userToRoleRepository.Update(userToRole);
            await _transactionManager.CommitAsync();

            return new UserToRoleModel(userToRole);
        }

        public async Task<UserToRoleModel> GetAsync(int userToRoleId)
        {
            var userToRole = await _userToRoleRepository.GetByIdAsync(userToRoleId);

            if (userToRole == null) return null;

            return new UserToRoleModel(userToRole);
        }

        private static void AddOrUpdate(UserToRole userToRoleEntity, EditUserToRoleModel userToRoleModel)
        {
            userToRoleEntity.UserId = userToRoleModel.UserId;
            userToRoleEntity.RoleId = userToRoleModel.RoleId;
        }

        public async Task<List<UserToRoleModel>> GetAllByRoleIdAsync(int roleId)
        {
            return (await _userToRoleRepository.GetAllByRoleIdAsync(roleId)).Select(s => new UserToRoleModel(s)).ToList();
        }

        public async Task<List<UserToRoleModel>> GetAllByUserIdAsync(int userId)
        {
            return (await _userToRoleRepository.GetAllByUserIdAsync(userId)).Select(s => new UserToRoleModel(s)).ToList();
        }
    }
}
