﻿using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using Otus.Beer.Planner.DataProvider.Transactions;
using Otus.Beer.Planner.UserGroups.Interfaces;
using Otus.Beer.Planner.UserGroups.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.UserGroups.Implementations
{
    public class UserGroupService : IUserGroupService
    {
        private readonly IUserGroupRepository _UserGroupRepository;
        private readonly ITransactionManager _transactionManager;

        public UserGroupService(IUserGroupRepository UserGroupRepository, ITransactionManager transactionManager)
        {
            _UserGroupRepository = UserGroupRepository;
            _transactionManager = transactionManager;
        }

        public async Task<UserGroupModel> AddAsync(EditUserGroupModel UserGroupModel)
        {
            var UserGroup = new UserGroup();
            
            AddOrUpdate(UserGroup, UserGroupModel);
            await _UserGroupRepository.AddAsync(UserGroup);
            await _transactionManager.CommitAsync();

            return new UserGroupModel(UserGroup);
        }

        public async Task<UserGroupModel> UpdateAsync(EditUserGroupModel UserGroupModel)
        {
            var UserGroup = await _UserGroupRepository.GetByIdAsync(UserGroupModel.Id.Value);

            if (UserGroup == null) throw new InvalidOperationException($"UserGroup with id '{UserGroupModel.Id} not found'");

            AddOrUpdate(UserGroup, UserGroupModel);
            _UserGroupRepository.Update(UserGroup);
            await _transactionManager.CommitAsync();

            return new UserGroupModel(UserGroup);
        }

        public async Task<UserGroupModel> UpdateAsync(EditUserGroupFromTelegramBotModel UserGroupModel)
        {
            var UserGroup = await _UserGroupRepository.GetByIdAsync(UserGroupModel.Id);

            if (UserGroup == null) throw new InvalidOperationException($"UserGroup with id '{UserGroupModel.Id} not found'");

            AddOrUpdate(UserGroup, UserGroupModel);
            _UserGroupRepository.Update(UserGroup);
            await _transactionManager.CommitAsync();

            return new UserGroupModel(UserGroup);
        }

        public async Task<UserGroupModel> GetAsync(int UserGroupId)
        {
            var UserGroup = await _UserGroupRepository.GetByIdAsync(UserGroupId);

            if (UserGroup == null) return null;

            return new UserGroupModel(UserGroup);
        }

        public async Task<List<UserGroupModel>> GetAllAsync()
        {
            var userGroups = await _UserGroupRepository.GetAllAsync();

            return userGroups.Select(s => new UserGroupModel(s)).ToList();
        }

        private static void AddOrUpdate(UserGroup UserGroupEntity, EditUserGroupModel UserGroupModel)
        {
            UserGroupEntity.Name = UserGroupModel.Name;
            UserGroupEntity.TelegramClientId = UserGroupModel.TelegramClientId;
        }

        private static void AddOrUpdate(UserGroup UserGroupEntity, EditUserGroupFromTelegramBotModel UserGroupModel)
        {
            UserGroupEntity.TelegramClientId = UserGroupModel.TelegramClientId;
        }

        public async Task<UserGroupModel> GetByTelegramClientIdAsync(string telegramClientId)
        {
            var UserGroup = await _UserGroupRepository.GetByTelegramClientIdAsync(telegramClientId);

            if (UserGroup == null) return null;

            return new UserGroupModel(UserGroup);
        }

        public async Task<UserGroupModel> GetByNameAsync(string name)
        {
            var userGroup = await _UserGroupRepository.GetByNameAsync(name);

            if (userGroup == null) return null;

            return new UserGroupModel(userGroup);
        }

        public async Task<UserGroupModel> GetUserGroupForUser(int userId)
        {
            var userGroup = await _UserGroupRepository.GetUserGroupForUser(userId);

            if (userGroup == null) return null;

            return new UserGroupModel(userGroup);
        }
    }
}
