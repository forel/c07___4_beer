﻿using Otus.Beer.Planner.UserGroups.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.UserGroups.Interfaces
{
    public interface IUserGroupService
    {
        Task<UserGroupModel> AddAsync(EditUserGroupModel UserGroup);
        Task<UserGroupModel> UpdateAsync(EditUserGroupModel UserGroup);
        Task<UserGroupModel> UpdateAsync(EditUserGroupFromTelegramBotModel UserGroup);
        Task<UserGroupModel> GetAsync(int UserGroupId);
        Task<List<UserGroupModel>> GetAllAsync();
        Task<UserGroupModel> GetByTelegramClientIdAsync(string telegramClientId);
        Task<UserGroupModel> GetByNameAsync(string name);
        Task<UserGroupModel> GetUserGroupForUser(int userId);
    }
}
