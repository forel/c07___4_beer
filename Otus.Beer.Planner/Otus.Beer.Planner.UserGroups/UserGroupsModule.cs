﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Beer.Planner.UserGroups.Implementations;
using Otus.Beer.Planner.UserGroups.Interfaces;

namespace Otus.Beer.Planner.UserGroups
{
    public static class UserGroupsModule
    {
        public static IServiceCollection AddUserGroupsModule(this IServiceCollection services)
        {
            services.AddScoped<IUserGroupService, UserGroupService>();

            return services;
        }
    }
}
