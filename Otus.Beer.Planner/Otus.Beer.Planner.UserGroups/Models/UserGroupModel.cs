﻿using Otus.Beer.Planner.DataProvider.Entities;

namespace Otus.Beer.Planner.UserGroups.Models
{
    public class UserGroupModel
    {
        public UserGroupModel(UserGroup entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            TelegramClientId = entity.TelegramClientId;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string TelegramClientId { get; set; }
    }
}
