﻿namespace Otus.Beer.Planner.UserGroups.Models
{
    public class EditUserGroupFromTelegramBotModel
    {
        public int Id { get; set; }
        public string TelegramClientId { get; set; }
    }
}
