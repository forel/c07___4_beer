﻿namespace Otus.Beer.Planner.UserGroups.Models
{
    public class EditUserGroupModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string TelegramClientId { get; set; }
    }
}
