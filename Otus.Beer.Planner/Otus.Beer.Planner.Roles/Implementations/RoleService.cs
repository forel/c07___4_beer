﻿using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using Otus.Beer.Planner.DataProvider.Transactions;
using Otus.Beer.Planner.Roles.Interfaces;
using Otus.Beer.Planner.Roles.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.Roles.Implementations
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;
        private readonly ITransactionManager _transactionManager;

        public RoleService(IRoleRepository roleRepository, ITransactionManager transactionManager)
        {
            _roleRepository = roleRepository;
            _transactionManager = transactionManager;
        }

        public async Task<RoleModel> GetAsync(int roleId)
        {
            var role = await _roleRepository.GetByIdAsync(roleId);

            if (role == null) return null;

            return new RoleModel(role);
        }

        public async Task<List<RoleModel>> GetAllAsync()
        {
            var roles = await _roleRepository.GetAllAsync();

            return roles.Select(s => new RoleModel(s)).ToList();
        }

        public async Task<RoleModel> GetRoleForUser(int userId)
        {
            var role = await _roleRepository.GetRoleForUser(userId);

            if (role == null) return null;

            return new RoleModel(role);
        }
    }
}
