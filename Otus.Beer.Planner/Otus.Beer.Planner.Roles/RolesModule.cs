﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Beer.Planner.Roles.Implementations;
using Otus.Beer.Planner.Roles.Interfaces;

namespace Otus.Beer.Planner.Roles
{
    public static class RolesModule
    {
        public static IServiceCollection AddRolesModule(this IServiceCollection services)
        {
            services.AddScoped<IRoleService, RoleService>();

            return services;
        }
    }
}
