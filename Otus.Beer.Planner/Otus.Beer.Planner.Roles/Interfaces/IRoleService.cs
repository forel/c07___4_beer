﻿using Otus.Beer.Planner.Roles.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.Roles.Interfaces
{
    public interface IRoleService
    {
        Task<RoleModel> GetAsync(int roleId);
        Task<List<RoleModel>> GetAllAsync();
        Task<RoleModel> GetRoleForUser(int userId);
    }
}
