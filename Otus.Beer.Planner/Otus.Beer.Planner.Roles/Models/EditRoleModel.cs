﻿namespace Otus.Beer.Planner.Roles.Models
{
    public class EditRoleModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
