﻿using Otus.Beer.Planner.DataProvider.Entities;

namespace Otus.Beer.Planner.Roles.Models
{
    public class RoleModel
    {
        public RoleModel(Role entity)
        {
            Id = entity.Id;
            Name = entity.Name;
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}
