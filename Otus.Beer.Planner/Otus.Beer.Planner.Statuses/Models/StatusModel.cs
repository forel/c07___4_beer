﻿using Otus.Beer.Planner.DataProvider.Entities;

namespace Otus.Beer.Planner.Statuses.Models
{
    public class StatusModel
    {
        public StatusModel(Status entity)
        {
            Id = entity.Id;
            Name = entity.Name;
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}
