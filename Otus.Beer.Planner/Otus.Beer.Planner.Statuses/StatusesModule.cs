﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Beer.Planner.Statuses.Implementations;
using Otus.Beer.Planner.Statuses.Interfaces;

namespace Otus.Beer.Planner.Statuses
{
    public static class StatusesModule
    {
        public static IServiceCollection AddStatusesModule(this IServiceCollection services)
        {
            services.AddScoped<IStatusService, StatusService>();

            return services;
        }
    }
}
