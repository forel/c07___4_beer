﻿using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using Otus.Beer.Planner.Statuses.Interfaces;
using Otus.Beer.Planner.Statuses.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.Statuses.Implementations
{
    public class StatusService : IStatusService
    {
        private readonly IStatusRepository _statusRepository;

        public StatusService(IStatusRepository statusRepository)
        {
            _statusRepository = statusRepository;
        }

        public async Task<List<StatusModel>> GetAllAsync()
        {
            var statuses = await _statusRepository.GetAllAsync();

            return statuses.Select(s => new StatusModel(s)).ToList();
        }
    }
}
