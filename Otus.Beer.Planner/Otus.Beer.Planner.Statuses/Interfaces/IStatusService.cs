﻿using Otus.Beer.Planner.Statuses.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.Statuses.Interfaces
{
    public interface IStatusService
    {
        Task<List<StatusModel>> GetAllAsync();
    }
}
