﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Beer.Planner.NotificationToUsers.Implementations;
using Otus.Beer.Planner.NotificationToUsers.Interfaces;

namespace Otus.Beer.Planner.NotificationToUsers
{
    public static class NotificationToUsersModule
    {
        public static IServiceCollection AddNotificationToUsersModule(this IServiceCollection services)
        {
            services.AddScoped<INotificationToUserService, NotificationToUserService>();

            return services;
        }
    }
}
