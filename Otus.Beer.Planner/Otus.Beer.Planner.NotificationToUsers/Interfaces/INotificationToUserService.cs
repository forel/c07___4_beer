﻿using Otus.Beer.Planner.NotificationToUsers.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.NotificationToUsers.Interfaces
{
    public interface INotificationToUserService
    {
        Task<NotificationToUserModel> AddAsync(EditNotificationToUserModel notificationToUser);
        Task<NotificationToUserModel> UpdateAsync(EditNotificationToUserModel notificationToUser);
        Task<NotificationToUserModel> UpdateAsync(EditNotificationToUserFromTelegramBotModel notificationToUser);
        Task<NotificationToUserModel> GetAsync(int notificationToUserId);

        Task<List<NotificationToUserForTelegramBotModel>> GetNewTelegramNotificationsToUsersAsync();
    }
}
