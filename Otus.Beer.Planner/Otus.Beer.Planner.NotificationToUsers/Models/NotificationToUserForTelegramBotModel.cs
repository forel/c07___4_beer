﻿using Otus.Beer.Planner.DataProvider.Entities;

namespace Otus.Beer.Planner.NotificationToUsers.Models
{
    public class UserForTelegramBotModel
    {
        public string TelegramClientId { get; set; }
    }

    public class NotificationForTelegramBotModel
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }

    public class NotificationToUserForTelegramBotModel
    {
        public NotificationToUserForTelegramBotModel(NotificationToUser entity)
        {
            Id = entity.Id;
            NotificationId = entity.NotificationId;
            UserId = entity.UserId;
            User = new UserForTelegramBotModel()
            {
                TelegramClientId = entity.User?.TelegramClientId
            };
            Notification = new NotificationForTelegramBotModel()
            {
                Content = entity.Notification?.Content,
                Title = entity.Notification?.Title
            };
        }

        public int Id { get; set; }
        public int NotificationId { get; set; }
        public int UserId { get; set; }
        public UserForTelegramBotModel User { get; set; }
        public NotificationForTelegramBotModel Notification { get; set; }
    }
}
