﻿using Otus.Beer.Planner.DataProvider.Entities;
using System;

namespace Otus.Beer.Planner.NotificationToUsers.Models
{
    public class NotificationToUserModel
    {
        public NotificationToUserModel(NotificationToUser entity)
        {
            Id = entity.Id;
            NotificationId = entity.NotificationId;
            UserId = entity.UserId;
            SendedDate = entity.SendedDate;
        }

        public int Id { get; set; }
        public int NotificationId { get; set; }
        public int UserId { get; set; }
        public DateTimeOffset? SendedDate { get; set; }
    }
}
