﻿using System;

namespace Otus.Beer.Planner.NotificationToUsers.Models
{
    public class EditNotificationToUserFromTelegramBotModel
    {
        public int Id { get; set; }
        public DateTimeOffset? SendedDate { get; set; }
    }
}
