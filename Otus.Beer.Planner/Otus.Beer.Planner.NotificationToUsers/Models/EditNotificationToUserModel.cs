﻿using System;

namespace Otus.Beer.Planner.NotificationToUsers.Models
{
    public class EditNotificationToUserModel
    {
        public int? Id { get; set; }
        public int NotificationId { get; set; }
        public int UserId { get; set; }
        public DateTimeOffset? SendedDate { get; set; }
    }
}
