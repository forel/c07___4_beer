﻿using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using Otus.Beer.Planner.DataProvider.Transactions;
using Otus.Beer.Planner.NotificationToUsers.Interfaces;
using Otus.Beer.Planner.NotificationToUsers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.NotificationToUsers.Implementations
{
    public class NotificationToUserService : INotificationToUserService
    {
        private readonly INotificationToUserRepository _notificationToUserRepository;
        private readonly ITransactionManager _transactionManager;

        public NotificationToUserService(INotificationToUserRepository notificationToUserRepository, ITransactionManager transactionManager)
        {
            _notificationToUserRepository = notificationToUserRepository;
            _transactionManager = transactionManager;
        }

        public async Task<NotificationToUserModel> AddAsync(EditNotificationToUserModel notificationToUserModel)
        {
            var notificationToUser = new NotificationToUser();

            AddOrUpdate(notificationToUser, notificationToUserModel);
            await _notificationToUserRepository.AddAsync(notificationToUser);
            await _transactionManager.CommitAsync();

            return new NotificationToUserModel(notificationToUser);
        }

        public async Task<NotificationToUserModel> UpdateAsync(EditNotificationToUserModel notificationToUserModel)
        {
            var notificationToUser = await _notificationToUserRepository.GetByIdAsync(notificationToUserModel.Id.Value);

            if (notificationToUser == null) throw new InvalidOperationException($"NotificationToUser with id '{notificationToUserModel.Id} not found'");

            AddOrUpdate(notificationToUser, notificationToUserModel);
            _notificationToUserRepository.Update(notificationToUser);
            await _transactionManager.CommitAsync();

            return new NotificationToUserModel(notificationToUser);
        }

        public async Task<NotificationToUserModel> UpdateAsync(EditNotificationToUserFromTelegramBotModel notificationToUserModel)
        {
            var notificationToUser = await _notificationToUserRepository.GetByIdAsync(notificationToUserModel.Id);

            if (notificationToUser == null) throw new InvalidOperationException($"NotificationToUser with id '{notificationToUserModel.Id} not found'");

            AddOrUpdate(notificationToUser, notificationToUserModel);
            _notificationToUserRepository.Update(notificationToUser);
            await _transactionManager.CommitAsync();

            return new NotificationToUserModel(notificationToUser);
        }

        public async Task<NotificationToUserModel> GetAsync(int notificationToUserId)
        {
            var notificationToUser = await _notificationToUserRepository.GetByIdAsync(notificationToUserId);

            if (notificationToUser == null) return null;

            return new NotificationToUserModel(notificationToUser);
        }

        private static void AddOrUpdate(NotificationToUser notificationToUserEntity, EditNotificationToUserModel notificationToUserModel)
        {
            notificationToUserEntity.NotificationId = notificationToUserModel.NotificationId;
            notificationToUserEntity.SendedDate = notificationToUserModel.SendedDate;
            notificationToUserEntity.UserId = notificationToUserModel.UserId;
        }

        private static void AddOrUpdate(NotificationToUser notificationToUserEntity, EditNotificationToUserFromTelegramBotModel notificationToUserModel)
        {
            notificationToUserEntity.SendedDate = notificationToUserModel.SendedDate;
        }

        public async Task<List<NotificationToUserForTelegramBotModel>> GetNewTelegramNotificationsToUsersAsync()
        {
            return (await _notificationToUserRepository.GetNewTelegramNotificationsToUsersAsync()).Select(t => new NotificationToUserForTelegramBotModel(t)).ToList();
        }
    }
}
