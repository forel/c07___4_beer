﻿using Otus.Beer.Planner.Auth.Models;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.Auth.Interfaces
{
    public interface IAuthService
    {
        Task SignUp(string email);
        Task<JWTModel> SignIn(string login, string password);
    }
}
