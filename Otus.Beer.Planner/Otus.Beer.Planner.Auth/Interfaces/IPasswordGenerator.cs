﻿namespace Otus.Beer.Planner.Auth.Interfaces
{
    public interface IPasswordGenerator
    {
        string Generate();
    }
}
