﻿namespace Otus.Beer.Planner.Auth.Models
{
    public class JWTModel
    {
        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }
        
        public int UserId { get; set; }
    }
}
