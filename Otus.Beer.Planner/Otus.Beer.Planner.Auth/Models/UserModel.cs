﻿using Otus.Beer.Planner.DataProvider.Entities;

namespace Otus.Beer.Planner.Auth.Models
{
    public class UserModel
    {
        public UserModel(User user)
        {
            Login = user.Login;
            Password = user.Password;
        }

        public string Login { get; set; }

        public string Password { get; set; }
    }
}
