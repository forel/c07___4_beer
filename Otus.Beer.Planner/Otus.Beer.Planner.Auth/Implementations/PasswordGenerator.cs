﻿using Otus.Beer.Planner.Auth.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace Otus.Beer.Planner.Auth.Implementations
{
    internal sealed class PasswordGenerator : IPasswordGenerator
    {
        private const string Uppercase = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
        private const string LowerCase = "abcdefghijkmnopqrstuvwxyz";
        private const string Numeric = "0123456789";
        private const string NonAlphaNumeric = "!@$#?_-";
        private const int RequiredLength = 8;

        private readonly char[] _randomChars;

        public PasswordGenerator()
        {
            _randomChars = BuildPool();
        }

        public string Generate()
        {
            using (var _cryptoProvider = new RNGCryptoServiceProvider())
            {
                var buffer = new byte[RequiredLength * 8];
                _cryptoProvider.GetBytes(buffer);

                var resultSet = new char[RequiredLength];
                int index = -1;

                Fill(++index, Uppercase.ToCharArray());
                Fill(++index, LowerCase.ToCharArray());
                Fill(++index, Numeric.ToCharArray());
                Fill(++index, NonAlphaNumeric.ToCharArray());

                for (int i = ++index; i < RequiredLength; i++)
                {
                    Fill(i, _randomChars);
                }

                void Fill(int i, char[] randomChars)
                {
                    ulong seed = BitConverter.ToUInt64(buffer, i * 8);
                    resultSet[i] = randomChars[seed % (uint)randomChars.Length];
                }

                return new string(resultSet);
            }
        }

        private char[] BuildPool()
        {
            var pool = new List<string>
            {
                Numeric,
                LowerCase,
                Uppercase,
                NonAlphaNumeric
            };

            return pool.Aggregate((i, j) => i + j).ToCharArray();
        }
    }
}
