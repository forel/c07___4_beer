﻿using Otus.Beer.Planner.Auth.Helpers;
using Otus.Beer.Planner.Auth.Interfaces;
using Otus.Beer.Planner.Auth.Models;
using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using Otus.Beer.Planner.DataProvider.Transactions;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.Auth.Implementations
{
    public class AuthService : IAuthService
    {
        private readonly IUserRepository _userRepository;
        private readonly ITransactionManager _transactionManager;
        private readonly IPasswordGenerator _passwordGenerator;
        private readonly ITokenService _tokenService;
        private readonly RegistrationNotifiationHelper _registrationNotifiationHelper;

        public AuthService(
            IUserRepository userRepository,
            ITransactionManager transactionManager,
            IPasswordGenerator passwordGenerator,
            ITokenService tokenService,
            RegistrationNotifiationHelper registrationNotifiationHelper)
        {
            _userRepository = userRepository;
            _transactionManager = transactionManager;
            _passwordGenerator = passwordGenerator;
            _tokenService = tokenService;
            _registrationNotifiationHelper = registrationNotifiationHelper;
        }

        public async Task SignUp(string email)
        {
            if (string.IsNullOrWhiteSpace(email)) throw new ArgumentNullException(nameof(email));

            var user = await CreateUser(email);

            await _registrationNotifiationHelper.Notify(email, user.Password);
        }

        public async Task<JWTModel> SignIn(string login, string password)
        {
            if (string.IsNullOrWhiteSpace(login)) throw new ArgumentNullException(nameof(login));
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentNullException(nameof(password));

            var user = await _userRepository.GetByLoginAsync(login);

            if (user == null || user.Password != password)
            {
                throw new InvalidOperationException();
            }

            var usersClaims = new[]
            {
                new Claim(ClaimTypes.Name, user.Login),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
            };

            var jwtToken = _tokenService.GenerateAccessToken(usersClaims);
            var refreshToken = _tokenService.GenerateRefreshToken();

            user.RefreshToken = refreshToken;
            _userRepository.Update(user);

            await _transactionManager.CommitAsync();

            return new JWTModel
            { 
                AccessToken = jwtToken,
                RefreshToken = refreshToken,
                UserId = user.Id
            };
        }

        private async Task<UserModel> CreateUser(string email)
        {
            var userEntity = new User
            {
                Email = email,
                Login = email,
                Password = _passwordGenerator.Generate()
            };

            await _userRepository.AddAsync(userEntity);
            await _transactionManager.CommitAsync();

            return new UserModel(userEntity);
        }
    }
}
