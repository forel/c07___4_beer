﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Beer.Planner.Auth.Helpers;
using Otus.Beer.Planner.Auth.Implementations;
using Otus.Beer.Planner.Auth.Interfaces;

namespace Otus.Beer.Planner.Auth
{
    public static class AuthModule
    {
        public static IServiceCollection AddAuthModule(this IServiceCollection services)
        {
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IPasswordGenerator, PasswordGenerator>();
            services.AddScoped<RegistrationNotifiationHelper>();

            return services;
        }
    }
}
