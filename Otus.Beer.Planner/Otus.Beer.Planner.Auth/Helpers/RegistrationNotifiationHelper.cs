﻿using Newtonsoft.Json.Linq;
using Otus.Beer.MailSender.Contracts;
using Otus.Beer.MailSender.Contracts.Enums;
using Otus.Beer.MessageBusBroker;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.Auth.Helpers
{
    public class RegistrationNotifiationHelper
    {
        private readonly IMessageBus _messageBus;

        public RegistrationNotifiationHelper(IMessageBus messageBus)
        {
            _messageBus = messageBus;
        }

        public async Task Notify(string email, string password)
        {
            JObject @params = new JObject
            {
                { "password", password }
            };

            await _messageBus.PublishAsync(new SendMessageEvent() 
            { 
                Email = email,
                TemplateId = (int)Templates.Registration,
                Params = @params
            });
        }
    }
}
