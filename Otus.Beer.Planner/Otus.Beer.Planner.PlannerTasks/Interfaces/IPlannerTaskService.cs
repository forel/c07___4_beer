﻿using Otus.Beer.Planner.PlannerTasks.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.PlannerTasks.Interfaces
{
    public interface IPlannerTaskService
    {
        Task<PlannerTaskModel> GetAsync(int taskId);
        Task<PlannerTaskModel> AddAsync(EditPlannerTaskModel taskModel);
        Task<PlannerTaskModel> UpdateAsync(EditPlannerTaskModel taskModel);
        Task DeleteAsync(int taskId);

        Task<List<PlannerTaskModel>> GetTasksForUserByPeriodAsync(int userId, int countDays);
        Task<List<PlannerTaskModel>> GetTasksByFilterAsync(PlannerTaskFilterModel filter);
    }
}
