﻿using AutoMapper;
using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Models;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using Otus.Beer.Planner.DataProvider.Transactions;
using Otus.Beer.Planner.PlannerTasks.Interfaces;
using Otus.Beer.Planner.PlannerTasks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.PlannerTasks.Implementations
{
    internal sealed class PlannerTaskService : IPlannerTaskService
    {
        private readonly IPlannerTaskRepository _plannerTaskRepository;
        private readonly ITransactionManager _transactionManager;
        private readonly IMapper _mapper;

        public PlannerTaskService(IPlannerTaskRepository plannerTaskRepository, ITransactionManager transactionManager, IMapper mapper)
        {
            _plannerTaskRepository = plannerTaskRepository;
            _transactionManager = transactionManager;
            _mapper = mapper;
        }

        public async Task<PlannerTaskModel> GetAsync(int taskId)
        {
            var task = await _plannerTaskRepository.GetByIdAsync(taskId);

            if (task == null) return null;

            return new PlannerTaskModel(task);
        }

        public async Task<PlannerTaskModel> AddAsync(EditPlannerTaskModel taskModel)
        {
            var task = new PlannerTask()
            {
                CreatedDate = DateTimeOffset.UtcNow
            };

            AddOrUpdate(task, taskModel);
            await _plannerTaskRepository.AddAsync(task);
            await _transactionManager.CommitAsync();

            return new PlannerTaskModel(task);
        }

        public async Task<PlannerTaskModel> UpdateAsync(EditPlannerTaskModel taskModel)
        {
            var task = await _plannerTaskRepository.GetByIdAsync(taskModel.Id.Value);

            if (task == null) throw new InvalidOperationException($"Task with id '{taskModel.Id} not found'");

            AddOrUpdate(task, taskModel);
            _plannerTaskRepository.Update();
            await _transactionManager.CommitAsync();

            return new PlannerTaskModel(task);
        }

        public async Task DeleteAsync(int taskId)
        {
            var task = await _plannerTaskRepository.GetByIdAsync(taskId);

            if (task == null) return;

            _plannerTaskRepository.Delete(task);
            await _transactionManager.CommitAsync();
        }

        private static void AddOrUpdate(PlannerTask taskEntity, EditPlannerTaskModel taskModel)
        {
            taskEntity.UserId = taskModel.UserId;
            taskEntity.Title = taskModel.Title;
            taskEntity.Description = taskModel.Description;
            taskEntity.Priority = taskModel.Priority;
            taskEntity.StatusId = taskModel.StatusId;
            taskEntity.StartDate = taskModel.StartDate;
            taskEntity.EndDate = taskModel.EndDate;
        }

        public async Task<List<PlannerTaskModel>> GetTasksForUserByPeriodAsync(int userId, int countDays)
        {
            return (await _plannerTaskRepository.GetTasksForUserByPeriodAsync(userId, countDays)).Select(t => new PlannerTaskModel(t)).ToList();
        }

        public async Task<List<PlannerTaskModel>> GetTasksByFilterAsync(PlannerTaskFilterModel filter)
        {
            var plannedTaskFilter = _mapper.Map<PlannerTaskFilter>(filter);
            return (await _plannerTaskRepository.GetTasksByFilterAsync(plannedTaskFilter)).Select(t => new PlannerTaskModel(t)).ToList();
        }
    }
}
