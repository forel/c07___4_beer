﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Beer.Planner.PlannerTasks.Implementations;
using Otus.Beer.Planner.PlannerTasks.Interfaces;

namespace Otus.Beer.Planner.PlannerTasks
{
    public static class PlannerTasksModule
    {
        public static IServiceCollection AddPlannerTasksModule(this IServiceCollection services)
        {
            services.AddScoped<IPlannerTaskService, PlannerTaskService>();
            services.AddAutoMapper(typeof(PlannerTasksModule));

            return services;
        }
    }
}
