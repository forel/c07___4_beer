﻿using AutoMapper;
using Otus.Beer.Planner.PlannerTasks.Models;
using Otus.Beer.Planner.DataProvider.Models;

namespace Otus.Beer.Planner.PlannerTasks.Mapper
{
    /// <summary>
    /// PlannerTaskFilter mapper
    /// </summary>
    public class PlannerTaskFilterProfile : Profile
    {
        public PlannerTaskFilterProfile()
        {
            CreateMap<PlannerTaskFilterModel, PlannerTaskFilter>();
        }
    }
}
