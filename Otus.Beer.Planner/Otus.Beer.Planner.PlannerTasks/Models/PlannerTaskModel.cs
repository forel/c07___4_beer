﻿using Otus.Beer.Planner.DataProvider.Entities;
using System;

namespace Otus.Beer.Planner.PlannerTasks.Models
{
    public class PlannerTaskModel
    {
        public PlannerTaskModel(PlannerTask entity)
        {
            Id = entity.Id;
            UserId = entity.UserId;
            UserLogin = entity.User?.Login;
            Title = entity.Title;
            Description = entity.Description;
            CreatedDate = entity.CreatedDate;
            Priority = entity.Priority;
            StatusId = entity.StatusId;
            StatusName = entity.Status?.Name;
            StartDate = entity.StartDate;
            EndDate = entity.EndDate;
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public string UserLogin { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public int Priority { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
    }
}
