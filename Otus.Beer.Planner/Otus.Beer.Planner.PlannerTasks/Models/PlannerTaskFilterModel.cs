﻿namespace Otus.Beer.Planner.PlannerTasks.Models
{
    public class PlannerTaskFilterModel
    {
        public int? Id { get; set; }
        public int? UserId { get; set; }
        public int? StatusId { get; set; }
        public string Text { get; set; }

        public int SearcherId { get; set; }
    }
}
