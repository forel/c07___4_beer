﻿using System.Text.Json.Serialization;

namespace Otus.Beer.Planner.DTO.Users
{
    public class UserDto
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("login")]
        public string Login { get; set; }

        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("phone")]
        public string Phone { get; set; }

        [JsonPropertyName("telegram_client_id")]
        public string TelegramClientId { get; set; }

        [JsonPropertyName("role_id")]
        public int? RoleId { get; set; }

        [JsonPropertyName("role_name")]
        public string RoleName { get; set; }

        [JsonPropertyName("user_group_id")]
        public int? UserGroupId { get; set; }

        [JsonPropertyName("user_group_name")]
        public string UserGroupName { get; set; }
    }
}
