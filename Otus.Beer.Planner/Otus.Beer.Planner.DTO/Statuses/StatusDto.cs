﻿using System.Text.Json.Serialization;

namespace Otus.Beer.Planner.DTO.Statuses
{
    public class StatusDto
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }
    }
}
