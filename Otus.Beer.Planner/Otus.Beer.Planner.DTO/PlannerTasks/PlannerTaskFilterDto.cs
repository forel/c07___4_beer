﻿using System.Text.Json.Serialization;

namespace Otus.Beer.Planner.DTO.PlannerTasks
{
    public class PlannerTaskFilterDto
    {
        [JsonPropertyName("user_id")]
        public int? UserId { get; set; }

        [JsonPropertyName("status_id")]
        public int? StatusId { get; set; }

        [JsonPropertyName("text")]
        public string Text { get; set; }
    }
}
