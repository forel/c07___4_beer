﻿using System.Text.Json.Serialization;

namespace Otus.Beer.Planner.DTO.UserGroups
{
    public class UserGroupDto
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }
    }
}
