﻿using System.Text.Json.Serialization;

namespace Otus.Beer.Planner.DTO.Auth
{
    public class JWTDto
    {
        [JsonPropertyName("access_token")]
        public string AccessToken { get; set; }

        [JsonPropertyName("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonPropertyName("user_id")]
        public int UserId { get; set; }
    }
}
