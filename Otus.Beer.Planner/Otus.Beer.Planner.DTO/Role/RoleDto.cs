﻿using System.Text.Json.Serialization;

namespace Otus.Beer.Planner.DTO.Roles
{
    public class RoleDto
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }
    }
}
