﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Beer.Planner.NotificationToGroups.Implementations;
using Otus.Beer.Planner.NotificationToGroups.Interfaces;

namespace Otus.Beer.Planner.NotificationToGroups
{
    public static class NotificationToGroupsModule
    {
        public static IServiceCollection AddNotificationToGroupsModule(this IServiceCollection services)
        {
            services.AddScoped<INotificationToGroupService, NotificationToGroupService>();

            return services;
        }
    }
}
