﻿using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using Otus.Beer.Planner.DataProvider.Transactions;
using Otus.Beer.Planner.NotificationToGroups.Interfaces;
using Otus.Beer.Planner.NotificationToGroups.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.NotificationToGroups.Implementations
{
    public class NotificationToGroupService : INotificationToGroupService
    {
        private readonly INotificationToGroupRepository _notificationToGroupRepository;
        private readonly ITransactionManager _transactionManager;

        public NotificationToGroupService(INotificationToGroupRepository notificationToGroupRepository, ITransactionManager transactionManager)
        {
            _notificationToGroupRepository = notificationToGroupRepository;
            _transactionManager = transactionManager;
        }

        public async Task<NotificationToGroupModel> AddAsync(EditNotificationToGroupModel notificationToGroupModel)
        {
            var notificationToGroup = new NotificationToGroup();

            AddOrUpdate(notificationToGroup, notificationToGroupModel);
            await _notificationToGroupRepository.AddAsync(notificationToGroup);
            await _transactionManager.CommitAsync();

            return new NotificationToGroupModel(notificationToGroup);
        }

        public async Task<NotificationToGroupModel> UpdateAsync(EditNotificationToGroupModel notificationToGroupModel)
        {
            var notificationToGroup = await _notificationToGroupRepository.GetByIdAsync(notificationToGroupModel.Id.Value);

            if (notificationToGroup == null) throw new InvalidOperationException($"NotificationToGroup with id '{notificationToGroupModel.Id} not found'");

            AddOrUpdate(notificationToGroup, notificationToGroupModel);
            _notificationToGroupRepository.Update(notificationToGroup);
            await _transactionManager.CommitAsync();

            return new NotificationToGroupModel(notificationToGroup);
        }

        public async Task<NotificationToGroupModel> UpdateAsync(EditNotificationToGroupFromTelegramBotModel notificationToGroupModel)
        {
            var notificationToGroup = await _notificationToGroupRepository.GetByIdAsync(notificationToGroupModel.Id);

            if (notificationToGroup == null) throw new InvalidOperationException($"NotificationToGroup with id '{notificationToGroupModel.Id} not found'");

            AddOrUpdate(notificationToGroup, notificationToGroupModel);
            _notificationToGroupRepository.Update(notificationToGroup);
            await _transactionManager.CommitAsync();

            return new NotificationToGroupModel(notificationToGroup);
        }

        public async Task<NotificationToGroupModel> GetAsync(int notificationToGroupId)
        {
            var notificationToGroup = await _notificationToGroupRepository.GetByIdAsync(notificationToGroupId);

            if (notificationToGroup == null) return null;

            return new NotificationToGroupModel(notificationToGroup);
        }

        private static void AddOrUpdate(NotificationToGroup notificationToGroupEntity, EditNotificationToGroupModel notificationToGroupModel)
        {
            notificationToGroupEntity.NotificationId = notificationToGroupModel.NotificationId;
            notificationToGroupEntity.SendedDate = notificationToGroupModel.SendedDate;
            notificationToGroupEntity.UserGroupId = notificationToGroupModel.UserGroupId;
        }

        private static void AddOrUpdate(NotificationToGroup notificationToGroupEntity, EditNotificationToGroupFromTelegramBotModel notificationToGroupModel)
        {
            notificationToGroupEntity.SendedDate = notificationToGroupModel.SendedDate;
        }

        public async Task<List<NotificationToGroupForTelegramBotModel>> GetNewTelegramNotificationsToGroupsAsync()
        {
            return (await _notificationToGroupRepository.GetNewTelegramNotificationsToGroupsAsync()).Select(t => new NotificationToGroupForTelegramBotModel(t)).ToList();
        }
    }
}
