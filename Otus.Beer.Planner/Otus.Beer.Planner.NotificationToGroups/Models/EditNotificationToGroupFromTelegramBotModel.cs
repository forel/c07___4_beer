﻿using System;

namespace Otus.Beer.Planner.NotificationToGroups.Models
{
    public class EditNotificationToGroupFromTelegramBotModel
    {
        public int Id { get; set; }
        public DateTimeOffset? SendedDate { get; set; }
    }
}
