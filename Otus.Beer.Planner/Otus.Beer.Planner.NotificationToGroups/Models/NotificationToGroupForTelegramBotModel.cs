﻿using Otus.Beer.Planner.DataProvider.Entities;

namespace Otus.Beer.Planner.NotificationToGroups.Models
{
    public class UserGroupForTelegramBotModel
    {
        public string TelegramClientId { get; set; }
    }

    public class NotificationForTelegramBotModel
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }

    public class NotificationToGroupForTelegramBotModel
    {
        public NotificationToGroupForTelegramBotModel(NotificationToGroup entity)
        {
            Id = entity.Id;
            NotificationId = entity.NotificationId;
            UserGroupId = entity.UserGroupId;
            UserGroup = new UserGroupForTelegramBotModel()
            {
                TelegramClientId = entity.UserGroup?.TelegramClientId
            };
            Notification = new NotificationForTelegramBotModel()
            {
                Content = entity.Notification?.Content,
                Title = entity.Notification?.Title
            };
        }

        public int Id { get; set; }
        public int NotificationId { get; set; }
        public int UserGroupId { get; set; }
        public UserGroupForTelegramBotModel UserGroup { get; set; }
        public NotificationForTelegramBotModel Notification { get; set; }
    }
}
