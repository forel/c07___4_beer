﻿using Otus.Beer.Planner.DataProvider.Entities;
using System;

namespace Otus.Beer.Planner.NotificationToGroups.Models
{
    public class NotificationToGroupModel
    {
        public NotificationToGroupModel(NotificationToGroup entity)
        {
            Id = entity.Id;
            NotificationId = entity.NotificationId;
            UserGroupId = entity.UserGroupId;
            SendedDate = entity.SendedDate;
        }

        public int Id { get; set; }
        public int NotificationId { get; set; }
        public int UserGroupId { get; set; }
        public DateTimeOffset? SendedDate { get; set; }
    }
}
