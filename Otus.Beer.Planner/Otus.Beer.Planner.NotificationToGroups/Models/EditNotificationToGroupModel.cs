﻿using System;

namespace Otus.Beer.Planner.NotificationToGroups.Models
{
    public class EditNotificationToGroupModel
    {
        public int? Id { get; set; }
        public int NotificationId { get; set; }
        public int UserGroupId { get; set; }
        public DateTimeOffset? SendedDate { get; set; }
    }
}
