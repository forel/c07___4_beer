﻿using Otus.Beer.Planner.NotificationToGroups.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.NotificationToGroups.Interfaces
{
    public interface INotificationToGroupService
    {
        Task<NotificationToGroupModel> AddAsync(EditNotificationToGroupModel notificationToGroup);
        Task<NotificationToGroupModel> UpdateAsync(EditNotificationToGroupModel notificationToGroup);
        Task<NotificationToGroupModel> UpdateAsync(EditNotificationToGroupFromTelegramBotModel notificationToGroup);
        Task<NotificationToGroupModel> GetAsync(int notificationToGroupId);

        Task<List<NotificationToGroupForTelegramBotModel>> GetNewTelegramNotificationsToGroupsAsync();
    }
}
