﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Beer.Planner.DataProvider.Entities;
using System.Collections.Generic;

namespace Otus.Beer.Planner.DataProvider.Configurations
{
    internal sealed class StatusConfiguration : IEntityTypeConfiguration<Status>
    {
        public void Configure(EntityTypeBuilder<Status> builder)
        {
            builder.HasData(
                new List<Status>
                {
                    new Status
                    { 
                        Id = 1,
                        Name = "New"
                    },
                    new Status
                    {
                        Id = 2,
                        Name = "InProgress"
                    },
                      new Status
                    {
                        Id = 3,
                        Name = "Done"
                    },
                }
           );
        }
    }
}
