﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Beer.Planner.DataProvider.Entities;
using System.Collections.Generic;

namespace Otus.Beer.Planner.DataProvider.Configurations
{
    internal sealed class RolesConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.HasData(
                new List<Role>
                {
                    new Role
                    { 
                        Id = 1,
                        Name = "User"
                    },
                    new Role
                    {
                        Id = 2,
                        Name = "Admin"
                    },
                }
           );
        }
    }
}
