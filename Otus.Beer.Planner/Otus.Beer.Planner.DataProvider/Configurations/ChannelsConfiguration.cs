﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Beer.Planner.DataProvider.Entities;
using System.Collections.Generic;

namespace Otus.Beer.Planner.DataProvider.Configurations
{
    internal sealed class ChannelsConfiguration : IEntityTypeConfiguration<Channel>
    {
        public void Configure(EntityTypeBuilder<Channel> builder)
        {
            builder.HasData(
                new List<Channel>
                {
                    new Channel
                    { 
                        Id = 1,
                        Name = "Telegram"
                    },
                }
           );
        }
    }
}
