﻿using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.DataProvider.Repositories.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> GetByLoginAsync(string login);
        Task<User> GetByTelegramClientIdAsync(string telegramClientId);
        Task<List<User>> GetUsersByFilterAsync(UserFilter filter);
    }
}
