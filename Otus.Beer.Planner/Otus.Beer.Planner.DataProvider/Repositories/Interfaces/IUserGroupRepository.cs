﻿using Otus.Beer.Planner.DataProvider.Entities;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.DataProvider.Repositories.Interfaces
{
    public interface IUserGroupRepository : IRepository<UserGroup>
    {
        Task<UserGroup> GetByNameAsync(string name);
        Task<UserGroup> GetByTelegramClientIdAsync(string telegramClientId);
        Task<UserGroup> GetUserGroupForUser(int userId);
    }
}
