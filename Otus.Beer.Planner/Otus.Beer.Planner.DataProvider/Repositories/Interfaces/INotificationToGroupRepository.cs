﻿using Otus.Beer.Planner.DataProvider.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.DataProvider.Repositories.Interfaces
{
    public interface INotificationToGroupRepository : IRepository<NotificationToGroup>
    {
        Task<List<NotificationToGroup>> GetNewTelegramNotificationsToGroupsAsync();
    }
}
