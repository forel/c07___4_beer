﻿using Otus.Beer.Planner.DataProvider.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.DataProvider.Repositories.Interfaces
{
    public interface IUserToRoleRepository : IRepository<UserToRole>
    {
        Task<List<UserToRole>> GetAllByRoleIdAsync(int roleId);
        Task<List<UserToRole>> GetAllByUserIdAsync(int userId);
    }
}
