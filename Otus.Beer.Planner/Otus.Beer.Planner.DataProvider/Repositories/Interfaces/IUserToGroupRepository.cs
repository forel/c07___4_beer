﻿using Otus.Beer.Planner.DataProvider.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.DataProvider.Repositories.Interfaces
{
    public interface IUserToGroupRepository : IRepository<UserToGroup>
    {
        Task<List<UserToGroup>> GetAllByUserGroupIdAsync(int userGroupId);
        Task<List<UserToGroup>> GetAllByUserIdAsync(int userId);
    }
}
