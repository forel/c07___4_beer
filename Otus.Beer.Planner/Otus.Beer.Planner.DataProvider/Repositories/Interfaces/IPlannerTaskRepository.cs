﻿using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.DataProvider.Repositories.Interfaces
{
    public interface IPlannerTaskRepository : IRepository<PlannerTask>
    {
        Task<List<PlannerTask>> GetTasksForUserByPeriodAsync(int userId, int countDays);

        Task<List<PlannerTask>> GetTasksByFilterAsync(PlannerTaskFilter filter);
    }
}
