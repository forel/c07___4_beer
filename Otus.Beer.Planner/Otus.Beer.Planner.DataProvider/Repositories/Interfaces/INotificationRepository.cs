﻿using Otus.Beer.Planner.DataProvider.Entities;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.DataProvider.Repositories.Interfaces
{
    public interface INotificationRepository : IRepository<Notification>
    {
    }
}
