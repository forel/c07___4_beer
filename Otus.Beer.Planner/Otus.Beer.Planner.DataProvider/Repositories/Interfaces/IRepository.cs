﻿using Otus.Beer.Planner.DataProvider.Entities.Intefraces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.DataProvider.Repositories.Interfaces
{
    public interface IRepository<TEntity> where TEntity : IBaseEntity
    {
        Task<TEntity> GetByIdAsync(int id);

        Task<IEnumerable<TEntity>> GetAllAsync();

        Task AddAsync(params TEntity[] entities);

        void Update(params TEntity[] entities);

        void Delete(params TEntity[] entities);
    }
}
