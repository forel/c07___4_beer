﻿using Otus.Beer.Planner.DataProvider.Entities;

namespace Otus.Beer.Planner.DataProvider.Repositories.Interfaces
{
    public interface IStatusRepository : IRepository<Status>
    {
    }
}
