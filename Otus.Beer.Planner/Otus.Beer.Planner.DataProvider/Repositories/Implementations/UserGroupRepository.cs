﻿using Microsoft.EntityFrameworkCore;
using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.DataProvider.Repositories.Implementations
{
    internal sealed class UserGroupRepository : Repository<UserGroup>, IUserGroupRepository
    {
        public UserGroupRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<UserGroup> GetByNameAsync(string name)
        {
            return await DbContext.UserGroups.FirstOrDefaultAsync(x => x.Name == name); 
        }

        public async Task<UserGroup> GetByTelegramClientIdAsync(string telegramClientId)
        {
            return await DbContext.UserGroups.FirstOrDefaultAsync(x => x.TelegramClientId == telegramClientId);
        }

        public async Task<UserGroup> GetUserGroupForUser(int userId)
        {
            return (await DbContext.UsersToGroups.Where(s => s.UserId == userId).Include(s => s.UserGroup).FirstOrDefaultAsync()).UserGroup;
        }
    }
}
