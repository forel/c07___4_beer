﻿using Microsoft.EntityFrameworkCore;
using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.DataProvider.Repositories.Implementations
{
    internal sealed class NotificationToGroupRepository : Repository<NotificationToGroup>, INotificationToGroupRepository
    {
        public NotificationToGroupRepository(ApplicationDbContext context): base(context)
        {
        }

        public async Task<List<NotificationToGroup>> GetNewTelegramNotificationsToGroupsAsync()
        {
            return await DbContext.NotificationsToGroups
                .Where(s => s.Notification.Channel.Name == "Telegram" && s.SendedDate == null && !string.IsNullOrEmpty(s.UserGroup.TelegramClientId))
                .Include(s => s.Notification)
                .Include(s => s.UserGroup)
                .ToListAsync();
        }
    }
}
