﻿using Microsoft.EntityFrameworkCore;
using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.DataProvider.Repositories.Implementations
{
    internal sealed class UserToGroupRepository : Repository<UserToGroup>, IUserToGroupRepository
    {
        public UserToGroupRepository(ApplicationDbContext context): base(context)
        {
        }

        public async Task<List<UserToGroup>> GetAllByUserGroupIdAsync(int userGroupId)
        {
            return await DbContext.UsersToGroups.Where(s => s.UserGroupId == userGroupId).ToListAsync();
        }

        public async Task<List<UserToGroup>> GetAllByUserIdAsync(int userId)
        {
            return await DbContext.UsersToGroups.Where(s => s.UserId == userId).ToListAsync();
        }
    }
}
