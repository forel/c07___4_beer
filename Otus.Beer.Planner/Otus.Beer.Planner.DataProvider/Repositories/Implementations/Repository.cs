﻿using Microsoft.EntityFrameworkCore;
using Otus.Beer.Planner.DataProvider.Entities.Intefraces;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.DataProvider.Repositories.Implementations
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IBaseEntity
    {
        protected readonly ApplicationDbContext DbContext;

        protected Repository(ApplicationDbContext dbContext)
        {
            this.DbContext = dbContext;
        }

        public async virtual Task AddAsync(params TEntity[] entities)
        {
            await DbContext.Set<TEntity>().AddRangeAsync(entities);
        }

        public async virtual Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await DbContext.Set<TEntity>().ToListAsync();
        }

        public async virtual Task<TEntity> GetByIdAsync(int id)
        {
            return await DbContext.Set<TEntity>().FindAsync(id);
        }

        public virtual void Update(params TEntity[] entities)
        {
            DbContext.Set<TEntity>().UpdateRange(entities);
        }

        public virtual void Delete(params TEntity[] entities)
        {
            var entityList = entities.ToList();
            entityList.ForEach(e => DbContext.Entry(e).State = EntityState.Deleted);
            DbContext.Set<TEntity>().RemoveRange(entityList);
        }
    }
}
