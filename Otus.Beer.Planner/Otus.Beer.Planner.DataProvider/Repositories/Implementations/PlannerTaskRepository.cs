﻿using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;
using Otus.Beer.Planner.DataProvider.Models;

namespace Otus.Beer.Planner.DataProvider.Repositories.Implementations
{
    internal sealed class PlannerTaskRepository : Repository<PlannerTask>, IPlannerTaskRepository
    {
        public PlannerTaskRepository(ApplicationDbContext context) : base(context)
        { 
        }

        public async Task<List<PlannerTask>> GetTasksForUserByPeriodAsync(int userId, int countDays)
        {
            return await DbContext.Tasks.Where(t => t.UserId == userId && t.StatusId < 3 && (countDays == 0 || (DateTime.Today <= t.StartDate && t.StartDate <= DateTime.Today.AddDays(countDays)))).ToListAsync();
        }

        public async Task<List<PlannerTask>> GetTasksByFilterAsync(PlannerTaskFilter filter)
        {
            string roleName = (
                from ur in DbContext.UsersToRoles
                join r in DbContext.Roles on ur.RoleId equals r.Id
                where ur.UserId == filter.SearcherId
                select r.Name).FirstOrDefault();

            if (string.IsNullOrEmpty(roleName))
            {
                throw new InvalidOperationException($"SearchUser with id '{filter.SearcherId}' not found");
            }

            return await DbContext.Tasks
                .Where(t =>
                    (!filter.Id.HasValue || filter.Id.Value == t.Id) &&
                    (!filter.UserId.HasValue || filter.UserId.Value == t.UserId) &&
                    (!filter.StatusId.HasValue || filter.StatusId.Value == t.StatusId) &&
                    (filter.Text == null || t.Title.Contains(filter.Text) || t.Description.Contains(filter.Text)) &&
                    (roleName == "Admin" || filter.SearcherId == t.UserId))
                .Include(t => t.User)
                .Include(t => t.Status)
                .ToListAsync();
        }
    }
}
