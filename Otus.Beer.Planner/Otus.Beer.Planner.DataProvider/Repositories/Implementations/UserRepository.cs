﻿using Microsoft.EntityFrameworkCore;
using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Models;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.DataProvider.Repositories.Implementations
{
    internal sealed class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<User> GetByLoginAsync(string login)
        {
            return await DbContext.Users.FirstOrDefaultAsync(x => x.Login == login); 
        }

        public async Task<User> GetByTelegramClientIdAsync(string telegramClientId)
        {
            return await DbContext.Users.FirstOrDefaultAsync(x => x.TelegramClientId == telegramClientId);
        }

        public async Task<List<User>> GetUsersByFilterAsync(UserFilter filter)
        {
            string roleName = (
                from ur in DbContext.UsersToRoles
                join r in DbContext.Roles on ur.RoleId equals r.Id
                where ur.UserId == filter.SearcherId
                select r.Name).FirstOrDefault();

            if (string.IsNullOrEmpty(roleName))
            {
                throw new InvalidOperationException($"SearchUser with id '{filter.SearcherId}' not found");
            }

            return await DbContext.Users
                .Where(t =>
                    (!filter.Id.HasValue || filter.Id.Value == t.Id) &&
                    (roleName == "Admin" || filter.SearcherId == t.Id))
                .ToListAsync();
        }
    }
}
