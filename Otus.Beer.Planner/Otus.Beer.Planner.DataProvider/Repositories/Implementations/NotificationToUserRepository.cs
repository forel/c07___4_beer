﻿using Microsoft.EntityFrameworkCore;
using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.DataProvider.Repositories.Implementations
{
    internal sealed class NotificationToUserRepository : Repository<NotificationToUser>, INotificationToUserRepository
    {
        public NotificationToUserRepository(ApplicationDbContext context): base(context)
        {
        }

        public async Task<List<NotificationToUser>> GetNewTelegramNotificationsToUsersAsync()
        {
            return await DbContext.NotificationsToUsers
                .Where(s => s.Notification.Channel.Name == "Telegram" && s.SendedDate == null && !string.IsNullOrEmpty(s.User.TelegramClientId))
                .Include(s => s.Notification)
                .Include(s => s.User)
                .ToListAsync();
        }
    }
}
