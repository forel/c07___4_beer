﻿using Microsoft.EntityFrameworkCore;
using Otus.Beer.Planner.DataProvider.Entities;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.Planner.DataProvider.Repositories.Implementations
{
    internal sealed class UserToRoleRepository : Repository<UserToRole>, IUserToRoleRepository
    {
        public UserToRoleRepository(ApplicationDbContext context): base(context)
        {
        }

        public async Task<List<UserToRole>> GetAllByRoleIdAsync(int roleId)
        {
            return await DbContext.UsersToRoles.Where(s => s.RoleId == roleId).ToListAsync();
        }

        public async Task<List<UserToRole>> GetAllByUserIdAsync(int userId)
        {
            return await DbContext.UsersToRoles.Where(s => s.UserId == userId).ToListAsync();
        }
    }
}
