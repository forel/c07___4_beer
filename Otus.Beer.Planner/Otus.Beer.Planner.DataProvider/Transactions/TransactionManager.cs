﻿using System.Threading.Tasks;

namespace Otus.Beer.Planner.DataProvider.Transactions
{
    internal sealed class TransactionManager : ITransactionManager
    {
        private readonly ApplicationDbContext _context;

        public TransactionManager(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task CommitAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
