﻿using System.Threading.Tasks;

namespace Otus.Beer.Planner.DataProvider.Transactions
{
    public interface ITransactionManager
    {
        Task CommitAsync();
    }
}
