﻿namespace Otus.Beer.Planner.DataProvider.Models
{
    public class UserFilter
    {
        public int? Id { get; set; }

        public int SearcherId { get; set; }
    }
}
