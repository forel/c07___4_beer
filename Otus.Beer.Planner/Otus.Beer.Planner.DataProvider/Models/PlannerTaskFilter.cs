﻿namespace Otus.Beer.Planner.DataProvider.Models
{
    public class PlannerTaskFilter
    {
        public int? Id { get; set; }
        public int? UserId { get; set; }
        public int? StatusId { get; set; }
        public string Text { get; set; }

        public int SearcherId { get; set; }
    }
}
