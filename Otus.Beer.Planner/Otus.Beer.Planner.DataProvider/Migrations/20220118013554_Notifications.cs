﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Beer.Planner.DataProvider.Migrations
{
    public partial class Notifications : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "SendedDate",
                table: "NotificationsToUsers",
                type: "timestamp with time zone",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "SendedDate",
                table: "NotificationsToGroups",
                type: "timestamp with time zone",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SendedDate",
                table: "NotificationsToUsers");

            migrationBuilder.DropColumn(
                name: "SendedDate",
                table: "NotificationsToGroups");
        }
    }
}
