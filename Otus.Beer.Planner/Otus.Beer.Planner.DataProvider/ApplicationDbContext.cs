﻿using Microsoft.EntityFrameworkCore;
using Otus.Beer.Planner.DataProvider.Configurations;
using Otus.Beer.Planner.DataProvider.Entities;

namespace Otus.Beer.Planner.DataProvider
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Channel> Channels { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<NotificationToGroup> NotificationsToGroups { get; set; }
        public DbSet<NotificationToUser> NotificationsToUsers { get; set; }
        public DbSet<PlannerTask> Tasks { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<UserToGroup> UsersToGroups { get; set; }
        public DbSet<UserToRole> UsersToRoles { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new StatusConfiguration());
            builder.ApplyConfiguration(new RolesConfiguration());
            builder.ApplyConfiguration(new ChannelsConfiguration());
        }
    }
}
