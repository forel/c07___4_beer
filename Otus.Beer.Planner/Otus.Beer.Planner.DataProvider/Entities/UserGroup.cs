﻿using System.ComponentModel.DataAnnotations;

namespace Otus.Beer.Planner.DataProvider.Entities
{
    public class UserGroup : BaseEntity
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        public string TelegramClientId { get; set; }
    }
}
