﻿
using System.ComponentModel.DataAnnotations;

namespace Otus.Beer.Planner.DataProvider.Entities
{
    public class Channel : BaseEntity
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
    }
}
