﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Beer.Planner.DataProvider.Entities
{
    public class NotificationToGroup : BaseEntity
    {
        [Required]
        public int NotificationId { get; set; }

        [Required]
        public int UserGroupId { get; set; }

        public DateTimeOffset? SendedDate { get; set; }

        [ForeignKey("NotificationId")]
        public Notification Notification { get; set; }

        [ForeignKey("UserGroupId")]
        public UserGroup UserGroup { get; set; }
    }
}
