﻿using System.ComponentModel.DataAnnotations;

namespace Otus.Beer.Planner.DataProvider.Entities.Intefraces
{
    public interface IBaseEntity
    {
        [Key]
        public int Id { get; set; }
    }
}
