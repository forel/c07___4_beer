﻿using Otus.Beer.Planner.DataProvider.Entities.Intefraces;
using System.ComponentModel.DataAnnotations;

namespace Otus.Beer.Planner.DataProvider.Entities
{
    public abstract class BaseEntity : IBaseEntity
    {
        [Key]
        public int Id { get; set; }
    }
}
