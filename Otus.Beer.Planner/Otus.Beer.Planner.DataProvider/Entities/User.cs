﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Beer.Planner.DataProvider.Entities
{
    public class User : BaseEntity
    {
        [Required]
        [MaxLength(100)]
        public string Login { get; set; }

        [Required]
        [MaxLength(100)]
        public string Password { get; set; }

        [MaxLength(100)]
        public string Email { get; set; }

        [MaxLength(50)]
        public string Phone { get; set; }

        public string TelegramClientId { get; set; }

        public string RefreshToken { get; set; }
    }
}
