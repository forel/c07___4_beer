﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Beer.Planner.DataProvider.Entities
{
    public class NotificationToUser : BaseEntity
    {
        [Required]
        public int NotificationId { get; set; }

        [Required]
        public int UserId { get; set; }

        public DateTimeOffset? SendedDate { get; set; }

        [ForeignKey("NotificationId")]
        public Notification Notification { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }
    }
}
