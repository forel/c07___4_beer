﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Beer.Planner.DataProvider.Entities
{
    public class UserToGroup : BaseEntity
    {
        [Required]
        public int UserId { get; set; }

        [Required]
        public int UserGroupId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        [ForeignKey("UserGroupId")]
        public UserGroup UserGroup { get; set; }
    }
}
