﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Beer.Planner.DataProvider.Entities
{
    public class PlannerTask : BaseEntity
    {
        [Required]
        public int UserId { get; set; }

        [Required]
        [MaxLength(150)]
        public string Title { get; set; }

        [MaxLength(5000)]
        public string Description { get; set; }

        [Required]
        public DateTimeOffset CreatedDate { get; set; }

        public int Priority { get; set; }

        [Required]
        public int StatusId { get; set; }

        public DateTimeOffset StartDate { get; set; }

        public DateTimeOffset EndDate { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        [ForeignKey("StatusId")]
        public Status Status { get; set; }
    }
}
