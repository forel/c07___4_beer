﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Beer.Planner.DataProvider.Repositories.Implementations;
using Otus.Beer.Planner.DataProvider.Repositories.Interfaces;
using Otus.Beer.Planner.DataProvider.Transactions;

namespace Otus.Beer.Planner.DataProvider
{
    public static class DataProviderModule
    {
        public static IServiceCollection AddDataProviderModule(this IServiceCollection services)
        {
            services.AddScoped<ITransactionManager, TransactionManager>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IPlannerTaskRepository, PlannerTaskRepository>();
            services.AddScoped<INotificationRepository, NotificationRepository>();
            services.AddScoped<INotificationToUserRepository, NotificationToUserRepository>();
            services.AddScoped<IUserGroupRepository, UserGroupRepository>();
            services.AddScoped<INotificationToGroupRepository, NotificationToGroupRepository>();
            services.AddScoped<IStatusRepository, StatusRepository>();
            services.AddScoped<IUserToGroupRepository, UserToGroupRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IUserToRoleRepository, UserToRoleRepository>();

            return services;
        }
    }
}
