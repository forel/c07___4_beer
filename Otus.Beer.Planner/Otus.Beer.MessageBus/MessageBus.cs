﻿using EasyNetQ;
using Otus.Beer.MessageBus.Options;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Otus.Beer.MessageBusBroker
{
    public class MessageBus : IMessageBus
    {
        private readonly IBus _bus;

        public MessageBus(IBus bus)
        {
            _bus = bus;
        }

        public async Task PublishAsync<TMessage>(TMessage message) where TMessage : class
        {
            await _bus.PubSub.PublishAsync(message);
        }

        public async Task SubscribeAsync<TMessage>(Func<TMessage, CancellationToken, Task> func, CancellationToken cancellationToken) where TMessage : class
        {
            await _bus.PubSub.SubscribeAsync("original", func, (config) =>
            {
                config.WithQueueName(GetQueue<TMessage>());
            }, cancellationToken);
        }

        public string GetQueue<TEvent>() where TEvent : class => $"{typeof(TEvent).FullName}";
    }
}
