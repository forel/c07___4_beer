﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Beer.MessageBus;
using Otus.Beer.MessageBus.Options;

namespace Otus.Beer.MessageBusBroker
{
    public static class MessageBusModule
    {
        public static IServiceCollection AddMessageBusModule(this IServiceCollection services, IConfiguration configuration)
        {
            var options = new MessageBrokerOptions();
            configuration.Bind("MessageBrokerOptions", options);

            var messageBusFactory = new MessageBusFactory();
            services.AddSingleton<IMessageBus>(messageBusFactory.CreateMessageBus(options));

            return services;
        }
    }
}
