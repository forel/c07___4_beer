﻿using EasyNetQ;
using Otus.Beer.MessageBus.Options;
using Otus.Beer.MessageBusBroker;
using System;
using System.Collections.Generic;

namespace Otus.Beer.MessageBus
{
    public class MessageBusFactory
    {
        public IMessageBus CreateMessageBus(MessageBrokerOptions options)
        {
            var configuration = new ConnectionConfiguration()
            {
                UserName = options.Username,
                Password = options.Password,
                VirtualHost = options.VirtualHost,
                Hosts = new List<HostConfiguration>()
                {
                    new HostConfiguration()
                    {
                        Host = options.Host,
                        Port = options.Port
                    }
                },
                Port = options.Port,
                Timeout = TimeSpan.FromSeconds(10),
                PublisherConfirms = true,
                PrefetchCount = (ushort)10
            };

            var messageBus = RabbitHutch.CreateBus(configuration, (e) => { });
            return new MessageBusBroker.MessageBus(messageBus);
        }
    }
}
