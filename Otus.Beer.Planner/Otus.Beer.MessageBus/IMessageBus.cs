﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Beer.MessageBusBroker
{
    public interface IMessageBus
    {
        Task PublishAsync<TMessage>(TMessage message) where TMessage : class;
        Task SubscribeAsync<TMessage>(Func<TMessage, CancellationToken, Task> func, CancellationToken cancellationToken = default) where TMessage : class;
    }
}
