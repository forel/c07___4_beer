﻿using Otus.Beer.Planner.TelegramBot.Models;
using Otus.Beer.Planner.Users.Interfaces;
using Otus.Beer.Planner.Users.Models;
using System;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;

namespace Otus.Beer.Planner.TelegramBot.Controllers
{
    public abstract class MessageAuthControllerBase: MessageControllerBase
    {
        private readonly IUserService _userService;

        public MessageAuthControllerBase(ITelegramBotClient bot, BotMessage message, IUserService userService) : base(bot, message)
        {
            _userService = userService;
        }

        protected async Task<UserModel> GetUser()
        {
            //Получаем пользователя User с полем TelegramClientId = _message.Chat.Id
            return await _userService.GetByTelegramClientIdAsync(_message.ChatId.ToString());
        }

        protected async Task SendMessageForUnknowUserAsync()
        {
            await _bot.SendTextMessageAsync(
                chatId: _message.ChatId,
                text: $"Для начала работы необходимо отправить команду:{Environment.NewLine}<b>/start [Login]</b>,{Environment.NewLine}где [Login] - логин пользователя в системе",
                parseMode: ParseMode.Html
            );
        }
    }
}
