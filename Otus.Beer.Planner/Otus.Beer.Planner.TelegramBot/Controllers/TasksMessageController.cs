﻿using Otus.Beer.Planner.PlannerTasks.Interfaces;
using Otus.Beer.Planner.PlannerTasks.Models;
using Otus.Beer.Planner.TelegramBot.Extensions;
using Otus.Beer.Planner.TelegramBot.Models;
using Otus.Beer.Planner.Users.Interfaces;
using Otus.Beer.Planner.Users.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;

namespace Otus.Beer.Planner.TelegramBot.Controllers
{
    class TasksMessageController : MessageAuthControllerBase
    {
        private readonly IPlannerTaskService _plannerTaskService;

        public TasksMessageController(ITelegramBotClient bot, BotMessage message, IUserService userService, IPlannerTaskService plannerTaskService) : base(bot, message, userService)
        {
            _plannerTaskService = plannerTaskService;
        }

        public override async Task MessageControl()
        {
            UserModel user = await GetUser();

            if (user == null)
            {
                await SendMessageForUnknowUserAsync();
                return;
            }

            if (_message.Params.Length == 1)
            {
                string[,] inlineKeyboard = { { "День", "/tasks_list_today" }, { "Неделя", "/tasks_list_week" }, { "Месяц", "/tasks_list_month" } };

                await SendTextMessageAsync("Выберите период", inlineKeyboard);
            }
            else
            {
                if (_message.Params[1] == "list")
                {
                    int countDays = _message.Params[2] switch
                    {
                        "today" => 1,
                        "week" => 7,
                        "month" => 30,
                        _ => 0
                    };

                    List<PlannerTaskModel> plannerTaskModels = await _plannerTaskService.GetTasksForUserByPeriodAsync(user.Id, countDays);

                    await SendTextMessageAsync(plannerTaskModels.ListTasksToString(_message.Params[2]));
                }
                else if (_message.Params[1] == "view")
                {
                    if (_message.Params.Length < 3)
                    {
                        await SendTextMessageAsync($"Не указан id задачи");
                        return;
                    }

                    int taskId = 0;
                    int.TryParse(_message.Params[2], out taskId);

                    if (taskId == 0)
                    {
                        await SendTextMessageAsync($"Не верно указан id задачи");
                        return;
                    }

                    PlannerTaskModel taskModel = await _plannerTaskService.GetAsync(taskId);

                    if (taskModel == null)
                    {
                        await SendTextMessageAsync($"Задача не найдена");
                        return;
                    }

                    await SendTextMessageAsync(taskModel.TaskToString());
                }
            }
        }
    }
}
