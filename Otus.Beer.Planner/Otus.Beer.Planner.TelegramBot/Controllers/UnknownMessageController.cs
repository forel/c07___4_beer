﻿using Otus.Beer.Planner.TelegramBot.Models;
using System.Threading.Tasks;
using Telegram.Bot;

namespace Otus.Beer.Planner.TelegramBot.Controllers
{
    public class UnknownMessageController : MessageControllerBase
    {
        public UnknownMessageController(ITelegramBotClient bot, BotMessage message) : base(bot, message)
        {
        }

        public override async Task MessageControl()
        {
            await SendTextMessageAsync("Неизвестная команда");
        }
    }
}
