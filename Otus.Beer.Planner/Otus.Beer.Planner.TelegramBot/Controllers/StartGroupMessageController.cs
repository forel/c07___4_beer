﻿using AutoMapper;
using Otus.Beer.Planner.TelegramBot.Models;
using Otus.Beer.Planner.UserGroups.Interfaces;
using Otus.Beer.Planner.UserGroups.Models;
using System;
using System.Threading.Tasks;
using Telegram.Bot;

namespace Otus.Beer.Planner.TelegramBot.Controllers
{
    public class StartGroupMessageController : MessageControllerBase
    {
        private readonly IUserGroupService _userGroupService;
        private readonly IMapper _mapper;

        public StartGroupMessageController(ITelegramBotClient bot, BotMessage message, IMapper mapper, IUserGroupService userGroupService) : base(bot, message)
        {
            _userGroupService = userGroupService;
            _mapper = mapper;
        }

        public override async Task MessageControl()
        {
            if (_message.Params.Length == 1)
            {
                await SendTextMessageAsync($"Команда должна иметь вид:{Environment.NewLine}<b>\"/startgroup [Name]\"</b>,{Environment.NewLine}где [Name] - название группы в системе");
            }
            else
            {
                string name = _message.Params[1];

                var group = await _userGroupService.GetByNameAsync(name);
                if (group == null)
                {
                    await SendTextMessageAsync($"Группа с названием {name} не найдена");
                    return;
                }

                if (!string.IsNullOrEmpty(group.TelegramClientId))
                {
                    await SendTextMessageAsync($"Группа с названием {name} уже в системе");
                    return;
                }

                group.TelegramClientId = _message.ChatId.ToString();

                var editUserGroupModel = _mapper.Map<EditUserGroupFromTelegramBotModel>(group);
                await _userGroupService.UpdateAsync(editUserGroupModel);

                await SendTextMessageAsync($"Группа успешно зарегистрирована");
            }
        }
    }
}
