﻿using AutoMapper;
using Otus.Beer.Planner.TelegramBot.Models;
using Otus.Beer.Planner.Users.Interfaces;
using Otus.Beer.Planner.Users.Models;
using System;
using System.Threading.Tasks;
using Telegram.Bot;

namespace Otus.Beer.Planner.TelegramBot.Controllers
{
    public class StartMessageController : MessageControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public StartMessageController(ITelegramBotClient bot, BotMessage message, IMapper mapper, IUserService userService) : base(bot, message)
        {
            _userService = userService;
            _mapper = mapper;
        }

        public override async Task MessageControl()
        {
            if (_message.Params.Length == 1)
            {
                await SendTextMessageAsync($"Команда должна иметь вид:{Environment.NewLine}<b>\"/start [Login]\"</b>,{Environment.NewLine}где [Login] - логин пользователя в системе");
            }
            else
            {
                string login = _message.Params[1];

                var user = await _userService.GetByLoginAsync(login);
                if (user == null)
                {
                    await SendTextMessageAsync($"Пользователь с логином {login} не найден");
                    return;
                }

                if (!string.IsNullOrEmpty(user.TelegramClientId))
                {
                    await SendTextMessageAsync($"Пользователь с логином {login} уже в системе");
                    return;
                }

                user.TelegramClientId = _message.ChatId.ToString();

                var editUserModel = _mapper.Map<EditUserFromTelegramBotModel>(user);
                await _userService.UpdateAsync(editUserModel);

                await SendTextMessageAsync($"Пользователь успешно зарегистрирован");
            }
        }
    }
}
