﻿using Otus.Beer.Planner.TelegramBot.Models;
using System.Threading.Tasks;
using Telegram.Bot;

namespace Otus.Beer.Planner.TelegramBot.Controllers
{
    public class SendMessageController : MessageControllerBase
    {
        public SendMessageController(ITelegramBotClient bot, BotMessage message) : base(bot, message)
        {
        }

        public override async Task MessageControl()
        {
            await SendTextMessageAsync(_message.Text);
        }
    }
}
