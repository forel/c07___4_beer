﻿using Otus.Beer.Planner.TelegramBot.Models;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace Otus.Beer.Planner.TelegramBot.Controllers
{
    public abstract class MessageControllerBase
    {
        protected readonly ITelegramBotClient _bot;
        protected readonly BotMessage _message;

        public MessageControllerBase(ITelegramBotClient bot, BotMessage message)
        {
            _bot = bot;
            _message = message;
        }

        public abstract Task MessageControl();

        protected async Task SendTextMessageAsync(string message)
        {
            await SendTextMessageAsync(message, null);
        }

        protected async Task SendTextMessageAsync(string message, string[,] inlineKeys = null)
        {
            InlineKeyboardMarkup inlineKeyboard = null;

            if (inlineKeys != null)
            {
                var keys = new InlineKeyboardButton[inlineKeys.Length / 2];

                for (int i = 0; i < inlineKeys.Length / 2; i++)
                {
                    keys[i] = InlineKeyboardButton.WithCallbackData(inlineKeys[i, 0], inlineKeys[i, 1]);
                }

                inlineKeyboard = new InlineKeyboardMarkup(keys);
            }

            await _bot.SendTextMessageAsync(
                chatId: _message.ChatId,
                text: message,
                parseMode: ParseMode.Html,
                replyMarkup: inlineKeyboard
            );
        }
    }
}
