﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;

namespace Otus.Beer.Planner.TelegramBot
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args, new TelegramBotServiceConfiguration()).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args, TelegramBotServiceConfiguration configuration) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService(options =>
                {
                    options.ServiceName = "TelegramBot Service";
                })
                .ConfigureServices((hostContext, services) =>
                {
                    configuration.ConfigureServices(hostContext, services);
                })
                .ConfigureAppConfiguration(builder =>
                {
                    builder.AddJsonFile("appsettings.json");
                });
    }
}
