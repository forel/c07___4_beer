﻿using Otus.Beer.Planner.NotificationToGroups.Interfaces;
using Otus.Beer.Planner.NotificationToGroups.Models;
using Otus.Beer.Planner.TelegramBot.Controllers;
using Otus.Beer.Planner.TelegramBot.Extensions;
using Otus.Beer.Planner.TelegramBot.Models;
using System;
using System.Threading.Tasks;
using Telegram.Bot;

namespace Otus.Beer.Planner.TelegramBot.Services
{
    public class SenderNotificationToGroupService
    {
        private readonly ITelegramBotClient _bot;
        private readonly INotificationToGroupService _notificationToGroupService;

        public SenderNotificationToGroupService(ITelegramBotClient bot, INotificationToGroupService notificationToGroupService)
        {
            _bot = bot;
            _notificationToGroupService = notificationToGroupService;
        }

        public async Task SendAsync()
        {
            var listNotifications = await _notificationToGroupService.GetNewTelegramNotificationsToGroupsAsync();
            if (listNotifications.Count > 0)
            {
                foreach (var notification in listNotifications)
                {
                    long chatId;
                    if (long.TryParse(notification.UserGroup.TelegramClientId, out chatId))
                    {
                        BotMessage message = new BotMessage()
                        {
                            ChatId = chatId,
                            Text = notification.NotificationToString()
                        };

                        SendMessageController sendMessageController = new SendMessageController(_bot, message);
                        await sendMessageController.MessageControl();

                        await _notificationToGroupService.UpdateAsync(new EditNotificationToGroupFromTelegramBotModel()
                        {
                            Id = notification.Id,
                            SendedDate = DateTimeOffset.Now
                        });
                    }
                }
            }
        }
    }
}
