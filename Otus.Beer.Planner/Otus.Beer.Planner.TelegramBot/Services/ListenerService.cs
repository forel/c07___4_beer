﻿using AutoMapper;
using Otus.Beer.Planner.PlannerTasks.Interfaces;
using Otus.Beer.Planner.TelegramBot.Controllers;
using Otus.Beer.Planner.TelegramBot.Extensions;
using Otus.Beer.Planner.TelegramBot.Models;
using Otus.Beer.Planner.UserGroups.Interfaces;
using Otus.Beer.Planner.Users.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Otus.Beer.Planner.TelegramBot.Services
{
    public class ListenerService
    {
        private readonly ITelegramBotClient _bot;
        private readonly IPlannerTaskService _plannerTaskService;
        private readonly IUserService _userService;
        private readonly IUserGroupService _userGroupService;
        private readonly IMapper _mapper;

        public ListenerService(ITelegramBotClient bot, IMapper mapper, IPlannerTaskService plannerTaskService, IUserService userService, IUserGroupService userGroupService)
        {
            _bot = bot;
            _plannerTaskService = plannerTaskService;
            _userService = userService;
            _userGroupService = userGroupService;
            _mapper = mapper;
        }

        public async Task RunAsync(CancellationToken ct)
        {
            var me = await _bot.GetMeAsync();
            Console.WriteLine($"Bot Id: {me.Id}. Bot Name: {me.FirstName}.");

            _bot.StartReceiving(
                new DefaultUpdateHandler(HandleUpdateAsync, HandleErrorAsync),
                ct);

            Console.WriteLine($"Start listening for @{me.Username}");
        }

        Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken ct)
        {
            var ErrorMessage = exception switch
            {
                ApiRequestException apiRequestException => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                _ => exception.ToString()
            };

            Console.WriteLine(ErrorMessage);
            return Task.CompletedTask;
        }

        async Task HandleUpdateAsync(ITelegramBotClient bot, Update update, CancellationToken ct)
        {
            if (update.Type != UpdateType.CallbackQuery)
            {
                if (update.Type != UpdateType.Message)
                    return;
                if (update.Message.Type != MessageType.Text)
                    return;
            }

            BotMessage message = update.TypeUpdateToBotMessage();

            MessageControllerBase control = message.Params[0] switch
            {
                "/start" => new StartMessageController(bot, message, _mapper, _userService),
                "/startgroup" => new StartGroupMessageController(bot, message, _mapper, _userGroupService),
                "/tasks" => new TasksMessageController(bot, message, _userService, _plannerTaskService),
                _ => new UnknownMessageController(bot, message)
            };
            await control.MessageControl();
        }
    }
}
