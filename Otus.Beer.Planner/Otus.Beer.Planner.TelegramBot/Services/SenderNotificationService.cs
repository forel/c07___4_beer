﻿using Microsoft.Extensions.Configuration;
using Otus.Beer.Planner.NotificationToGroups.Interfaces;
using Otus.Beer.Planner.NotificationToUsers.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;

namespace Otus.Beer.Planner.TelegramBot.Services
{
    public class SenderNotificationService
    {
        private readonly IConfiguration _configuration;
        private readonly ITelegramBotClient _bot;
        private readonly INotificationToUserService _notificationToUserService;
        private readonly INotificationToGroupService _notificationToGroupService;

        public SenderNotificationService(IConfiguration configuration, ITelegramBotClient bot, INotificationToUserService notificationToUserService, INotificationToGroupService notificationToGroupService)
        {
            _configuration = configuration;
            _bot = bot;
            _notificationToUserService = notificationToUserService;
            _notificationToGroupService = notificationToGroupService;
        }

        public async Task RunAsync(CancellationToken ct)
        {
            int timeOut = 15;
            int.TryParse(_configuration["AppSettings:SenderTimeOutInSeconds"], out timeOut);

            while (!ct.IsCancellationRequested)
            {
                SenderNotificationToUserService senderNotificationToUserService = new SenderNotificationToUserService(_bot, _notificationToUserService);
                await senderNotificationToUserService.SendAsync();

                SenderNotificationToGroupService senderNotificationToGroupService = new SenderNotificationToGroupService(_bot, _notificationToGroupService);
                await senderNotificationToGroupService.SendAsync();

                await Task.Delay(TimeSpan.FromSeconds(timeOut), ct);
            }
        }
    }
}
