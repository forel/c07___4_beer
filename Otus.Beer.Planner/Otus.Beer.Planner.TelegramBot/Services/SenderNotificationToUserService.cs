﻿using Otus.Beer.Planner.NotificationToUsers.Interfaces;
using Otus.Beer.Planner.NotificationToUsers.Models;
using Otus.Beer.Planner.TelegramBot.Controllers;
using Otus.Beer.Planner.TelegramBot.Extensions;
using Otus.Beer.Planner.TelegramBot.Models;
using System;
using System.Threading.Tasks;
using Telegram.Bot;

namespace Otus.Beer.Planner.TelegramBot.Services
{
    public class SenderNotificationToUserService
    {
        private readonly ITelegramBotClient _bot;
        private readonly INotificationToUserService _notificationToUserService;

        public SenderNotificationToUserService(ITelegramBotClient bot, INotificationToUserService notificationToUserService)
        {
            _bot = bot;
            _notificationToUserService = notificationToUserService;
        }

        public async Task SendAsync()
        {
            var listNotifications = await _notificationToUserService.GetNewTelegramNotificationsToUsersAsync();
            if (listNotifications.Count > 0)
            {
                foreach (var notification in listNotifications)
                {
                    long chatId;
                    if (long.TryParse(notification.User.TelegramClientId, out chatId))
                    {
                        BotMessage message = new BotMessage()
                        {
                            ChatId = chatId,
                            Text = notification.NotificationToString()
                        };

                        SendMessageController sendMessageController = new SendMessageController(_bot, message);
                        await sendMessageController.MessageControl();

                        await _notificationToUserService.UpdateAsync(new EditNotificationToUserFromTelegramBotModel()
                        {
                            Id = notification.Id,
                            SendedDate = DateTimeOffset.Now
                        });
                    }
                }
            }
        }
    }
}
