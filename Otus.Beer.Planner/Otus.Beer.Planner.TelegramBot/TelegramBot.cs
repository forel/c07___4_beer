﻿using Otus.Beer.Planner.PlannerTasks.Interfaces;
using Otus.Beer.Planner.Users.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using AutoMapper;
using Microsoft.Extensions.Hosting;
using Otus.Beer.Planner.TelegramBot.Services;
using Microsoft.Extensions.Configuration;
using Otus.Beer.Planner.NotificationToUsers.Interfaces;
using Otus.Beer.Planner.UserGroups.Interfaces;
using Otus.Beer.Planner.NotificationToGroups.Interfaces;

namespace Otus.Beer.Planner.TelegramBot
{
    public class TelegramBot : BackgroundService
    {
        private readonly IConfiguration _configuration;
        private readonly ITelegramBotClient _bot;
        private readonly IPlannerTaskService _plannerTaskService;
        private readonly IUserService _userService;
        private readonly INotificationToUserService _notificationToUserService;
        private readonly IUserGroupService _userGroupService;
        private readonly INotificationToGroupService _notificationToGroupService;
        private readonly IMapper _mapper;

        public TelegramBot(IConfiguration configuration,
            ITelegramBotClient bot,
            IMapper mapper,
            IPlannerTaskService plannerTaskService,
            IUserService userService,
            INotificationToUserService notificationToUserService,
            IUserGroupService userGroupService,
            INotificationToGroupService notificationToGroupService)
        {
            _configuration = configuration;
            _bot = bot;
            _plannerTaskService = plannerTaskService;
            _userService = userService;
            _notificationToUserService = notificationToUserService;
            _userGroupService = userGroupService;
            _notificationToGroupService = notificationToGroupService;
            _mapper = mapper;
        }

        protected override async Task ExecuteAsync(CancellationToken ct)
        {
            ListenerService telegramBotListener = new ListenerService(_bot, _mapper, _plannerTaskService, _userService, _userGroupService);
            await telegramBotListener.RunAsync(ct);

            SenderNotificationService telegramBotSender = new SenderNotificationService(_configuration, _bot, _notificationToUserService, _notificationToGroupService);
            await telegramBotSender.RunAsync(ct);

            Console.ReadLine();
        }
    }
}
