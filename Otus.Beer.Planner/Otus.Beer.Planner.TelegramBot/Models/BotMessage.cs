﻿namespace Otus.Beer.Planner.TelegramBot.Models
{
    public class BotMessage
    {
        public long ChatId { get; set; }
        public string Text { get; set; }
        public string[] Params { get; set; }
    }
}
