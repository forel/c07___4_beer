﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Beer.Planner.DataProvider;
using Otus.Beer.Planner.NotificationToGroups;
using Otus.Beer.Planner.NotificationToUsers;
using Otus.Beer.Planner.PlannerTasks;
using Otus.Beer.Planner.UserGroups;
using Otus.Beer.Planner.Users;
using System;
using Telegram.Bot;

namespace Otus.Beer.Planner.TelegramBot
{
    public class TelegramBotServiceConfiguration
    {
        public void ConfigureServices(HostBuilderContext context, IServiceCollection services)
        {
            string connectionString = Environment.GetEnvironmentVariable("ConnectionString") ?? context.Configuration["ConnectionStrings:DefaultConnection"];
            string accessToken = context.Configuration["TelegramBot:ACCESS_TOKEN"];

            services.AddHostedService<TelegramBot>()
                    .AddDbContext<ApplicationDbContext>(options => options.UseNpgsql(connectionString))
                    .AddDataProviderModule()
                    .AddPlannerTasksModule()
                    .AddUsersModule()
                    .AddNotificationToUsersModule()
                    .AddUserGroupsModule()
                    .AddNotificationToGroupsModule()
                    .AddAutoMapper(typeof(Program))
                    .AddSingleton<ITelegramBotClient>(new TelegramBotClient(accessToken));
        }
    }
}
