﻿using AutoMapper;
using Otus.Beer.Planner.Users.Models;

namespace Otus.Beer.Planner.TelegramBot.Mapper
{
    /// <summary>
    /// User mapper
    /// </summary>
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserModel, EditUserFromTelegramBotModel>();
        }
    }
}
