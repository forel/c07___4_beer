﻿using AutoMapper;
using Otus.Beer.Planner.UserGroups.Models;

namespace Otus.Beer.Planner.TelegramBot.Mapper
{
    /// <summary>
    /// User mapper
    /// </summary>
    public class UserGroupProfile : Profile
    {
        public UserGroupProfile()
        {
            CreateMap<UserGroupModel, EditUserGroupFromTelegramBotModel>();
        }
    }
}
