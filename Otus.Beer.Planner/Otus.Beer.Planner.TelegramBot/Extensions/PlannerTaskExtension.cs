﻿using Otus.Beer.Planner.PlannerTasks.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Beer.Planner.TelegramBot.Extensions
{
    public static class PlannerTaskExtension
    {
        private const string DATE_FORMAT = "dd.MM.yyyy";

        public static string TaskToString(this PlannerTaskModel taskModel)
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.AppendLine($"<b>{taskModel.Title}</b>");
            stringBuilder.AppendLine($"<i>Создано:</i> {taskModel.CreatedDate.ToString(DATE_FORMAT)}");
            stringBuilder.AppendLine($"<i>Описание:</i> {taskModel.Description}");
            stringBuilder.AppendLine($"<i>Приоритет:</i> {taskModel.Priority}");
            stringBuilder.AppendLine($"<i>Начало:</i> {taskModel.StartDate.ToString(DATE_FORMAT)}");
            stringBuilder.AppendLine($"<i>Окончание:</i> {taskModel.EndDate.ToString(DATE_FORMAT)}");
            stringBuilder.AppendLine();

            return stringBuilder.ToString();
        }

        public static string ListTasksToString(this List<PlannerTaskModel> taskModels, string period)
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(period switch
            {
                "today" => $"Список заданий на сегодня",
                "week" => $"Список заданий на неделю",
                "month" => $"Список заданий на месяц",
                _ => "Неизвестный параметр"
            });

            if (taskModels.Count > 0)
            {
                stringBuilder.AppendLine($":{Environment.NewLine}");

                int i = 0;
                foreach (var taskModel in taskModels)
                {
                    i++;
                    stringBuilder.AppendLine($"{i}. {taskModel.Title.Substring(0, Math.Min(50, taskModel.Title.Length))}");
                    stringBuilder.AppendLine($"<i>Создано:</i> {taskModel.CreatedDate.ToString(DATE_FORMAT)}");
                    stringBuilder.AppendLine($"<i>Описание:</i> {taskModel.Description.Substring(0, Math.Min(100, taskModel.Description.Length))}");
                    stringBuilder.AppendLine($"<i>Подробнее:</i> /tasks_view_{taskModel.Id}");
                    stringBuilder.AppendLine();
                }
            }
            else
            {
                stringBuilder.AppendLine(" пуст");
            }

            return stringBuilder.ToString();
        }
    }
}
