﻿using Otus.Beer.Planner.TelegramBot.Models;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Otus.Beer.Planner.TelegramBot.Extensions
{
    public static class TelegramBotExtension
    {
        public static BotMessage TypeUpdateToBotMessage(this Update update)
        {
            if (update.Type == UpdateType.CallbackQuery)
            {
                update.Message = update.CallbackQuery.Message;
                update.Message.Text = update.CallbackQuery.Data;
            }

            update.Message.Text = update.Message.Text.Replace("_", " ");

            string[] messageParams = update.Message.Text.Split(" ");
            if (messageParams.Length == 0) messageParams = new string[1] { "" };

            return new BotMessage()
            {
                ChatId = update.Message.Chat.Id,
                Text = update.Message.Text,
                Params = messageParams
            };

        }
    }
}
