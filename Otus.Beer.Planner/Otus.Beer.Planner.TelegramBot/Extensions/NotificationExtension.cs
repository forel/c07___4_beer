﻿using Otus.Beer.Planner.NotificationToGroups.Models;
using Otus.Beer.Planner.NotificationToUsers.Models;
using System;

namespace Otus.Beer.Planner.TelegramBot.Extensions
{
    public static class NotificationExtension
    {
        private static string NotificationToString(string title, string content)
        {
            return $"<b>Новое уведомление:</b>{Environment.NewLine}<i>{title}</i>{Environment.NewLine}{content.Substring(0, Math.Min(200, content.Length))}";
        }

        public static string NotificationToString(this NotificationToUserForTelegramBotModel notification)
        {
            return NotificationToString(notification.Notification.Title, notification.Notification.Content);
        }

        public static string NotificationToString(this NotificationToGroupForTelegramBotModel notification)
        {
            return NotificationToString(notification.Notification.Title, notification.Notification.Content);
        }
    }
}
