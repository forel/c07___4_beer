﻿using Newtonsoft.Json.Linq;
using System;

namespace Otus.Beer.MailSender.Contracts
{
    public class SendMessageEvent
    {
        public string Email { get; set; }
        public int TemplateId { get; set; }
        public JObject Params { get; set; }
    }
}
