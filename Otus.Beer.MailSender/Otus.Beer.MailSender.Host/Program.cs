using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.MailSender.Host
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args, new MailSenderConfigeration()).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args, MailSenderConfigeration configeration)
        {
            return Microsoft.Extensions.Hosting.Host.CreateDefaultBuilder(args)
               .ConfigureServices((hostContext, services) =>
               {
                   configeration.ConfigureServices(hostContext, services);
               }).ConfigureAppConfiguration(
                 (context, builder) => ConfigureAppConfiguration(builder));
        }

        public static IConfigurationBuilder ConfigureAppConfiguration(IConfigurationBuilder builder)
        {
            builder
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
              .AddEnvironmentVariables();

            return builder;
        }
    }
}
