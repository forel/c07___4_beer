using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Otus.Beer.MailSender.Contracts;
using Otus.Beer.MailSender.Host.Subscribers;
using Otus.Beer.MessageBusBroker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Beer.MailSender.Host
{
    internal sealed class MailSenderService : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IMessageBus _messageBus;
        private readonly ILogger<MailSenderService> _logger;

        public MailSenderService(IServiceProvider serviceProvider, IMessageBus messageBus, ILogger<MailSenderService> logger)
        {
            _messageBus = messageBus;
            _serviceProvider = serviceProvider;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.CompletedTask;
        }

        public override async Task StartAsync(CancellationToken cancellationToken)
        {
            await SubscribeHandlersAsync(cancellationToken);
        }

        private async Task SubscribeHandlersAsync(CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested) return;

            await _messageBus.SubscribeAsync<SendMessageEvent>(_serviceProvider.GetService<SendMessageEventHandler>().Handle, cancellationToken);
        }
    }
}
