﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Beer.MailSender.Contracts;
using Otus.Beer.SendInBlue;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Beer.MailSender.Host.Subscribers
{
    public class SendMessageEventHandler 
    {
        private readonly IServiceScopeFactory _scopeFactory;

        public SendMessageEventHandler(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public async Task Handle(SendMessageEvent message, CancellationToken cancellationToken)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var sendInBlueAdapter = scope.ServiceProvider.GetRequiredService<ISendInBlueAdapter>();

                await sendInBlueAdapter.SendEmailAsync(message.Email, message.TemplateId, message.Params);
            }
        }
    }
}
