﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Beer.MailSender.Host.Subscribers;
using Otus.Beer.MessageBusBroker;
using Otus.Beer.SendInBlue;
using Otus.Beer.SendInBlue.Options;

namespace Otus.Beer.MailSender.Host
{
    public class MailSenderConfigeration
    {
        public void ConfigureServices(HostBuilderContext context, IServiceCollection services)
        {        
            services.AddHostedService<MailSenderService>()
                    .AddMessageBusModule(context.Configuration)
                    .AddSingleton<SendMessageEventHandler>() 
                    .AddSendInBlueModulee(context.Configuration);
        }
    }
}
