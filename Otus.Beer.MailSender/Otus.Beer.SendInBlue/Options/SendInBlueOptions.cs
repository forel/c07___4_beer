﻿namespace Otus.Beer.SendInBlue.Options
{
    internal sealed class SendInBlueOptions
    {
        public string ApiKey { get; set; }
    }
}
