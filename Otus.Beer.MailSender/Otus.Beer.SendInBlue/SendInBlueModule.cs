﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Beer.SendInBlue.Options;

namespace Otus.Beer.SendInBlue
{
    public static class SendInBlueModule
    {
        public static IServiceCollection AddSendInBlueModulee(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<SendInBlueOptions>(configuration.GetSection("SendInBlue"));
            services.AddScoped<ISendInBlueAdapter, SendInBlueAdapter>();

            return services;
        }
    }
}
