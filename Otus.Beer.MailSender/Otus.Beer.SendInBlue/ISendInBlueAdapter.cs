﻿using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace Otus.Beer.SendInBlue
{
    public interface ISendInBlueAdapter
    {
        Task SendEmailAsync(string email, int templateId, JObject @params);
    }
}
