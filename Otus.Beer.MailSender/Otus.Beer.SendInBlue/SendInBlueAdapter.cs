﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Otus.Beer.SendInBlue.Options;
using sib_api_v3_sdk.Api;
using sib_api_v3_sdk.Client;
using sib_api_v3_sdk.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Beer.SendInBlue
{
    internal sealed class SendInBlueAdapter : ISendInBlueAdapter
    {
        private readonly SendInBlueOptions _sendInBlueOptions;

        public SendInBlueAdapter(IOptions<SendInBlueOptions> sendInBlueOptions)
        {
            _sendInBlueOptions = sendInBlueOptions.Value;
        }

        public async Task SendEmailAsync(string email, int templateId, JObject @params)
        {
            Configuration.Default.ApiKey.Add("api-key", _sendInBlueOptions.ApiKey);

            var apiInstance = new TransactionalEmailsApi();
           
            string ToEmail = email;
            SendSmtpEmailTo smtpEmailTo = new SendSmtpEmailTo(ToEmail);
            List<SendSmtpEmailTo> To = new List<SendSmtpEmailTo>();
            To.Add(smtpEmailTo);
         
            Dictionary<string, object> _parmas = new Dictionary<string, object>();
            _parmas.Add("params", _parmas); 

            try
            {
                var sendSmtpEmail = new SendSmtpEmail(to: To, templateId: templateId, _params: @params);
                CreateSmtpEmail result = await apiInstance.SendTransacEmailAsync(sendSmtpEmail);
                
                Console.WriteLine(result.ToJson());
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }
    }
}
