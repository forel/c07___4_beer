import React, { Component } from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import './style.css';

export class NavMenu extends Component {
    static displayName = NavMenu.name;

    constructor (props) {
        super(props);

        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            collapsed: true
        };
    }

    toggleNavbar () {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }

    render() {
        let menuItems;
        let menuAdmin;

        if (this.props.isAdmin) {
            menuAdmin = (
                <NavItem>
                    <NavLink tag={Link} className="text-dark" to="/admin_users">Пользователи</NavLink>
                </NavItem>
            );
        }

        if (this.props.isLogin) {
            menuItems = (
                <ul className="navbar-nav flex-grow">
                    <NavItem>
                        <NavLink tag={Link} className="text-dark" to="/">Задачи</NavLink>
                    </NavItem>
                    {menuAdmin}
                    <NavItem>
                        <NavLink tag={Link} className="text-dark" to="/logout" onClick={this.props.onUserLogout}>Выход</NavLink>
                    </NavItem>
                </ul>
            );
        }


        return (
            <header>
                <Navbar className="navbar-expand-sm navbar-toggleable-sm ng-white border-bottom box-shadow mb-3" light>
                    <Container>
                        <NavbarBrand tag={Link} to={this.props.isLogin ? "/" : "/login"}>Планировщик задач</NavbarBrand>
                        <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
                        <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={!this.state.collapsed} navbar>
                            {menuItems}
                        </Collapse>
                    </Container>
                </Navbar>
            </header>
        );
    }
}
