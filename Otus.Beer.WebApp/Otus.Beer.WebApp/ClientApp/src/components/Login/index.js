import React, { Component } from 'react';
import fetchJson from '../../utils/fetch-json';
import './style.css';

export class Login extends Component {
    static displayName = Login.name;

    constructor(props) {
        super(props);

        this.state = { loginName: "", loginPassword: "" };
    }

    onInputChangeHandler = (e) => {
        const { id, value } = e.currentTarget;
        this.state[id] = value;
    }

    onButtonClickHandler = async (e) => {
        e.preventDefault();

        const { PLANNER_API_URL } = window;

        let url = new URL("auth/signin", PLANNER_API_URL);
        url.searchParams.append("login", this.state.loginName);
        url.searchParams.append("password", this.state.loginPassword);

        try {
            const result = await fetchJson(url, {
                method: "POST",
                headers: {
                    "Content-Type": "text/plain",
                }
            });

            if (result.access_token) {
                localStorage.setItem("access_token", result.access_token);

                url = new URL("users/" + result.user_id, PLANNER_API_URL);

                const user = await fetchJson(url, {
                    method: "GET",
                    headers: {
                        "Content-Type": "text/plain",
                        "Authorization": "Bearer " + result.access_token
                    }
                });
                localStorage.setItem("is_admin", user?.role_name === "Admin");

                this.props.onUserLogin();
            }
        }
        catch (error) {
            console.log(error.message);
        }

        return false;
    }

    render() {
        return (
            <main className="form-login">
                <form>
                    <h3 className="h4 mb-3 fw-normal">Введите логин и пароль</h3>
                    <input type="login" className="form-control" id="loginName" placeholder="Логин" onChange={this.onInputChangeHandler} />
                    <input type="password" className="form-control" id="loginPassword" placeholder="Пароль" onChange={this.onInputChangeHandler} />
                    <button className="btn btn-primary w-100" type="submit" onClick={this.onButtonClickHandler}>Вход</button>
                </form>
            </main>
        );
    }
}
