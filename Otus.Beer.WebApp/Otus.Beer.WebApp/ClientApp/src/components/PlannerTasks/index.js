import React, { Component } from 'react';
import fetchJson from '../../utils/fetch-json';
import { ButtonAdd } from './ButtonAdd';
import { ButtonEdit } from './ButtonEdit';
import { ButtonDel } from './ButtonDel';
import { PanelFilter } from './PanelFilter';

export class PlannerTasks extends Component {
    static displayName = PlannerTasks.name;

    constructor(props) {
        super(props);

        this.state = {
            plannerTasks: [],
            filter: {
                UserId: null,
                StatusId: null,
                Text: null
            }
        };

        this.onRefreshPlannerTasks = this.onRefreshPlannerTasks.bind(this);
        this.onButtonEditClickHandler = this.onButtonEditClickHandler.bind(this);
        this.GetPlannerTasks = this.GetPlannerTasks.bind(this);
        this.onChangeFilter = this.onChangeFilter.bind(this);
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Загрузка задач...</em></p>
            : this.renderPlannerTaskTable(this.state.plannerTasks);

        return (
            <div className="row">
                <div className="col-3">
                    <div className="pb-4">
                        <h5>Операции</h5>
                        <ButtonAdd onRefreshPlannerTasks={this.onRefreshPlannerTasks} />
                    </div>
                    <PanelFilter onUserLogout={this.props.onUserLogout} onRefreshPlannerTasks={this.onRefreshPlannerTasks} onChangeFilter={this.onChangeFilter} />
                </div>
                <div className="col-9">
                    <h5>Запланированные задачи</h5>
                    {contents}
                </div>
            </div>
        );
    }

    renderPlannerTaskTable = (plannerTasks) => {
        const rows = plannerTasks.map(task => {
            const start_date = new Date(task.start_date);
            const end_date = new Date(task.end_date);

            return (
                <tr key={task.id}>
                    <td>{task.user_login}</td>
                    <td>{task.title}</td>
                    <td>{start_date.toLocaleString()}</td>
                    <td>{end_date.toLocaleString()}</td>
                    <td>{task.status_name}</td>
                    <td>
                        <ButtonEdit onRefreshPlannerTasks={this.onRefreshPlannerTasks} task={task} />
                        <ButtonDel onRefreshPlannerTasks={this.onRefreshPlannerTasks} task_id={task.id} />
                    </td>
                </tr>
            )
        });

        return (
            <table className="table table-striped table-hover" aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Пользователь</th>
                        <th>Заголовок</th>
                        <th>Дата начала</th>
                        <th>Дата окончания</th>
                        <th>Статус</th>
                        <th>Операции</th>
                    </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </table>
        );
    }

    async componentDidMount() {
        this.GetPlannerTasks();
    }

    onRefreshPlannerTasks = () => {
        this.GetPlannerTasks();
    }

    onButtonEditClickHandler = (e) => {
        e.preventDefault();

        this.setState({ isModalOpen: true });
    }

    onChangeFilter = (filter) => {
        this.setState({ filter: filter }, () => { this.GetPlannerTasks() });
    }

    GetPlannerTasks = async () => {
        const { PLANNER_API_URL } = window;
        const access_token = localStorage.getItem("access_token");

        if (!access_token) return;

        try {
            const url = new URL("plannerTasks/search", PLANNER_API_URL);
            Object.entries(this.state.filter).map(([key, value]) => {
                if (value !== null) {
                    url.searchParams.append(key, value);
                }
            });

            let plannerTasks = await fetchJson(url, {
                method: "GET",
                headers: {
                    "Content-Type": "text/plain",
                    "Authorization": "Bearer " + access_token
                }
            });

            plannerTasks = plannerTasks.map(task => {
                task.start_date = new Date(task.start_date);
                task.end_date = new Date(task.end_date);

                return task;
            });

            this.setState({ plannerTasks: plannerTasks, loading: false });
        }
        catch (err) {
            if (err.response?.status === 401) {
                this.props.onUserLogout();
            }
        }
    }
}
