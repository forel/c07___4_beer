﻿import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import fetchJson from '../../../utils/fetch-json';

export class PanelFilter extends Component {
    static displayName = PanelFilter.name;

    constructor(props) {
        super(props);

        this.state = {
            listUsers: [],
            listStatuses: [],
            filter: {
                UserId: null,
                StatusId: null,
                Text: null
            }
        };

        this.onInputChangeHandler = this.onInputChangeHandler.bind(this);
    }

    render() {
        const optionsUsers = this.state.listUsers.map(item => {
            return (<option key={item.id} value={item.id}>{item.login}</option>);
        });

        const optionsStatuses = this.state.listStatuses.map(item => {
            return (<option key={item.id} value={item.id}>{item.name}</option>);
        });

        return (
            <div>
                <h5>Фильтр</h5>
                <Form>
                    <FormGroup>
                        <Label for="users">
                            Пользователь
                        </Label>
                        <Input id="users" name="UserId" type="select" onChange={this.onSelectChangeHandler}>
                            <option key="0" value="0">Любой</option>
                            {optionsUsers}
                        </Input>
                    </FormGroup>
                    <FormGroup>
                        <Label for="statuses">
                            Статус
                        </Label>
                        <Input id="statuses" name="StatusId" type="select" onChange={this.onSelectChangeHandler}>
                            <option key="0" value="0">Любой</option>
                            {optionsStatuses}
                        </Input>
                    </FormGroup>
                    <FormGroup>
                        <Label for="title">
                            Текст
                        </Label>
                        <Input id="text" name="Text" type="text" onChange={this.onInputChangeHandler} />
                    </FormGroup>
                    <FormGroup className="text-right">
                        <Button color="primary" onClick={this.onButtonClickHandler}>Поиск</Button>
                    </FormGroup>
                </Form>
            </div>
        );
    }

    async componentDidMount() {
        const { PLANNER_API_URL } = window;
        const access_token = localStorage.getItem("access_token");

        if (!access_token) return;

        try {
            let url = new URL("users/getall", PLANNER_API_URL);

            const promiseUsers = fetchJson(url, {
                method: "GET",
                headers: {
                    "Content-Type": "text/plain",
                    "Authorization": "Bearer " + access_token
                }
            });

            url = new URL("statuses/getall", PLANNER_API_URL);

            const promiseStatuses = fetchJson(url, {
                method: "GET",
                headers: {
                    "Content-Type": "text/plain",
                    "Authorization": "Bearer " + access_token
                }
            });

            const [listUsers, listStatuses] = await Promise.all([promiseUsers, promiseStatuses]);

            this.setState({ listUsers: listUsers, listStatuses: listStatuses });
        }
        catch (err) {
            if (err.response.status === 401) {
                this.props.onUserLogout();
            }
        }
    }

    onSelectChangeHandler = (e) => {
        const { name, value } = e.currentTarget;

        let filter = this.state.filter;
        filter[name] = value !== "0" ? value : null;

        this.setState({ filter: filter });
    }

    onInputChangeHandler = (e) => {
        const { name, value } = e.currentTarget;

        let filter = this.state.filter;
        filter[name] = value;

        this.setState({ filter: filter });
    }

    onButtonClickHandler = async (e) => {
        this.props.onChangeFilter(this.state.filter);
    }
}
