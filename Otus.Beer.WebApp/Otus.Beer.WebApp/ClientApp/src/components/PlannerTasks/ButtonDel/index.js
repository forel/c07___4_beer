import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import fetchJson from '../../../utils/fetch-json';

export class ButtonDel extends Component {
    static displayName = ButtonDel.name;

    constructor (props) {
        super(props);

        this.state = { isModalOpen: false };

        this.onDelClickHandler = this.onDelClickHandler.bind(this);
        this.onToggleHandler = this.onToggleHandler.bind(this);
        this.onButtonClickHandler = this.onButtonClickHandler.bind(this);
    }

    render() {
        let modalDel = "";
        if (this.state.isModalOpen) {
            modalDel = (
                <Modal isOpen={true} toggle={this.onToggleHandler}>
                    <ModalHeader>
                        Удаление задачи
                    </ModalHeader>
                    <ModalBody>
                        <h5>Действительно желаете удалить задачу?</h5>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={this.onDelClickHandler}>
                            Удалить
                        </Button>
                        {' '}
                        <Button onClick={this.onToggleHandler}>
                            Отмена
                    </Button>
                    </ModalFooter>
                </Modal>
            );
        };

        return (
            <>
                <Button color="danger" size="sm" outline onClick={this.onButtonClickHandler}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x-lg" viewBox="0 0 16 16">
                        <path fillRule="evenodd" d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z" />
                        <path fillRule="evenodd" d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z" />
                    </svg>
                </Button>
                {modalDel}
            </>
        );
    }

    onDelClickHandler = async (e) => {
        const { PLANNER_API_URL } = window;
        const access_token = localStorage.getItem("access_token");

        if (!access_token) return;

        try {
            let url = new URL("plannerTasks/" + this.props.task_id, PLANNER_API_URL);

            await fetch(url, {
                method: "DELETE",
                headers: {
                    "Content-Type": "text/plain",
                    "Authorization": "Bearer " + access_token
                }
            });

            this.setState({ isModalOpen: false });
            this.props.onRefreshPlannerTasks();
        }
        catch (err) {
            if (err.response.status === 401) {
                this.props.onUserLogout();
            }
        }
    }

    onToggleHandler = (e) => {
        e.preventDefault();

        this.setState({ isModalOpen: false });
    }

    onButtonClickHandler = (e) => {
        e.preventDefault();

        this.setState({ isModalOpen: true });
    }
}
