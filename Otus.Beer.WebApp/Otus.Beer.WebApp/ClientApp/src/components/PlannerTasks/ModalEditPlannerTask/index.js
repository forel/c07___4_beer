import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';
import fetchJson from '../../../utils/fetch-json';
import DatePicker, { registerLocale, setDefaultLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import ru from 'date-fns/locale/ru';
registerLocale('ru', ru)

export class ModalEditPlannerTask extends Component {
    static displayName = ModalEditPlannerTask.name;

    constructor (props) {
        super(props);

        this.state = {
            task: props.task,
            listUsers: [],
            listStatuses: [],
            listPriorities: []
        };

        this.state.isAdd = this.state.task.id === 0 || !this.state.task.id;

        this.onInputChangeHandler = this.onInputChangeHandler.bind(this);
        this.onStartDateChangeHandler = this.onStartDateChangeHandler.bind(this);
        this.onEndDateChangeHandler = this.onEndDateChangeHandler.bind(this);
        this.onSaveClickHandler = this.onSaveClickHandler.bind(this);
    }

    render() {
        const optionsUsers = this.state.listUsers.map(item => {
            return (<option key={item.id} value={item.id}>{item.login}</option>);
        });

        const optionsStatuses = this.state.listStatuses.map(item => {
            return (<option key={item.id} value={item.id}>{item.name}</option>);
        });

        const optionsPriorities = this.state.listPriorities.map(item => {
            return (<option key={item} value={item}>{item}</option>);
        });

        return (
            <Modal isOpen={true} toggle={this.onToggleHandler}>
                <ModalHeader>
                    {this.state.isAdd ? "Новая задача" : "Редактирование задачи"}
                </ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup>
                            <Label for="users">
                                Пользователь
                            </Label>
                            <Input id="users" name="user_id" type="select" onChange={this.onInputChangeHandler} value={this.state.task.user_id}>
                                {optionsUsers}
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for="title">
                                Заголовок
                            </Label>
                            <Input id="title" name="title" type="text" value={this.state.task.title} onChange={this.onInputChangeHandler} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="description">
                                Описание задачи
                            </Label>
                            <Input id="description" name="description" type="textarea" rows="4" value={this.state.task.description} onChange={this.onInputChangeHandler} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="priority">
                                Приоритет
                            </Label>
                            <Input id="priority" name="priority" type="select" onChange={this.onInputChangeHandler} value={this.state.task.priority}>
                                {optionsPriorities}
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for="statuses">
                                Статус
                            </Label>
                            <Input id="statuses" name="status_id" type="select" onChange={this.onInputChangeHandler} value={this.state.task.status_id}>
                                {optionsStatuses}
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Container>
                                <Row>
                                    <Col>
                                        <Label for="start_date">
                                            Дата начала
                                        </Label>
                                        <DatePicker locale="ru" selected={this.state.task.start_date} onChange={this.onStartDateChangeHandler} />
                                    </Col>
                                    <Col>
                                        <Label for="end_date">
                                            Дата окончания
                                        </Label>
                                        <DatePicker locale="ru" selected={this.state.task.end_date} onChange={this.onEndDateChangeHandler} />
                                    </Col>
                                </Row>
                            </Container>
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.onSaveClickHandler}>
                        {this.state.isAdd ? "Создать" : "Сохранить"}
                    </Button>
                    {' '}
                    <Button onClick={this.onToggleHandler}>
                        Отмена
                    </Button>
                </ModalFooter>
            </Modal>
        );
    }

    onToggleHandler = (e) => {
        e.preventDefault();

        this.props.onModalClosed();
    }

    onInputChangeHandler = (e) => {
        const { name, value } = e.currentTarget;

        let task = this.state.task;
        task[name] = value;

        this.setState({ task: task });
    }

    onStartDateChangeHandler = (date) => {
        let task = this.state.task;
        task.start_date = date;

        this.setState({ task: task });
    }

    onEndDateChangeHandler = (date) => {
        let task = this.state.task;
        task.end_date = date;

        this.setState({ task: task });
    }

    onSaveClickHandler = async (e) => {
        const { PLANNER_API_URL } = window;
        const access_token = localStorage.getItem("access_token");

        if (!access_token) return;

        try {
            if (this.state.isAdd) {
                let url = new URL("plannerTasks", PLANNER_API_URL);

                const newTask = await fetchJson(url, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + access_token
                    },
                    body: JSON.stringify(this.state.task)
                });
            }
            else {
                let url = new URL(`plannerTasks/{this.state.task.id}`, PLANNER_API_URL);

                const newTask = await fetchJson(url, {
                    method: "PUT",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + access_token
                    },
                    body: JSON.stringify(this.state.task)
                });
            }

            this.props.onModalClosed();
            this.props.onRefreshPlannerTasks();
        }
        catch (err) {
            if (err.response.status === 401) {
                this.props.onUserLogout();
            }
        }
    }

    async componentDidMount() {
        const { PLANNER_API_URL } = window;
        const access_token = localStorage.getItem("access_token");

        if (!access_token) return;

        try {
            let url = new URL("users/getall", PLANNER_API_URL);

            const promiseUsers = fetchJson(url, {
                method: "GET",
                headers: {
                    "Content-Type": "text/plain",
                    "Authorization": "Bearer " + access_token
                }
            });

            url = new URL("statuses/getall", PLANNER_API_URL);

            const promiseStatuses = fetchJson(url, {
                method: "GET",
                headers: {
                    "Content-Type": "text/plain",
                    "Authorization": "Bearer " + access_token
                }
            });

            const [listUsers, listStatuses] = await Promise.all([promiseUsers, promiseStatuses]);
            const listPriorities = [1, 2, 3, 4, 5];

            const task = this.state.task;
            if (this.state.isAdd) {
                task.user_id = listUsers[0].id;
                task.status_id = listStatuses[0].id;
                task.priority = listPriorities[0];
                task.start_date = new Date();
                task.end_date = new Date();
            }

            this.setState({ task: task, listUsers: listUsers, listStatuses: listStatuses, listPriorities: listPriorities });
        }
        catch (err) {
            if (err.response.status === 401) {
                this.props.onUserLogout();
            }
        }
    }
}
