import React, { Component } from 'react';
import { Button } from 'reactstrap';
import { ModalEditPlannerTask } from "../ModalEditPlannerTask/";

export class ButtonAdd extends Component {
    static displayName = ButtonAdd.name;

    constructor (props) {
        super(props);

        this.state = { isModalOpen: false };

        this.onModalClosed = this.onModalClosed.bind(this);
    }

    onButtonClickHandler = (e) => {
        e.preventDefault();

        this.setState({ isModalOpen: true });
    }

    onModalClosed = () => {
        this.setState({ isModalOpen: false });
    }

    render() {
        let modalEdit = "";
        if (this.state.isModalOpen) {
            modalEdit = (
                <ModalEditPlannerTask onModalClosed={this.onModalClosed} task={{}} onRefreshPlannerTasks={this.props.onRefreshPlannerTasks} />
            );
        };

        return (
            <>
                <Button color="primary" className="w-100" onClick={this.onButtonClickHandler}>Новая задача</Button>
                {modalEdit}
            </>
        );
    }
}
