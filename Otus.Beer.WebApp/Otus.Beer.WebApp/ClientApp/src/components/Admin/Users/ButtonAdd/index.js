import React, { Component } from 'react';
import { Button } from 'reactstrap';
import { ModalEditUser } from "../ModalEditUser/";

export class ButtonAdd extends Component {
    static displayName = ButtonAdd.name;

    constructor (props) {
        super(props);

        this.state = { isModalOpen: false };

        this.onModalClosed = this.onModalClosed.bind(this);
    }

    onButtonClickHandler = (e) => {
        e.preventDefault();

        this.setState({ isModalOpen: true });
    }

    onModalClosed = () => {
        this.setState({ isModalOpen: false });
    }

    render() {
        let modalEdit = "";
        if (this.state.isModalOpen) {
            modalEdit = (
                <ModalEditUser onModalClosed={this.onModalClosed} user_id={0} onRefreshUsers={this.props.onRefreshUsers} />
            );
        };

        return (
            <>
                <Button color="primary" className="w-100" onClick={this.onButtonClickHandler}>Новый пользователь</Button>
                {modalEdit}
            </>
        );
    }
}
