﻿import React, { Component } from 'react';
import fetchJson from '../../../utils/fetch-json';
import { ButtonAdd } from './ButtonAdd';
import { ButtonEdit } from './ButtonEdit';

export class Users extends Component {
    static displayName = Users.name;

    constructor(props) {
        super(props);

        this.state = {
            users: []
        };

        this.onRefreshUsers = this.onRefreshUsers.bind(this);
        this.onButtonEditClickHandler = this.onButtonEditClickHandler.bind(this);
        this.GetUsers = this.GetUsers.bind(this);
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Загрузка пользователей...</em></p>
            : this.renderUserTable(this.state.users);

        return (
            <div className="row">
                <div className="col-3">
                    <div className="pb-4">
                        <h5>Операции</h5>
                        <ButtonAdd onRefreshUsers={this.onRefreshUsers} />
                    </div>
                </div>
                <div className="col-9">
                    <h5>Пользователи</h5>
                    {contents}
                </div>
            </div>
        );
    }

    renderUserTable = (users) => {
        const rows = users.map(user => {
            return (
                <tr key={user.id}>
                    <td>{user.login}</td>
                    <td>{user.email}</td>
                    <td>{user.phone}</td>
                    <td>
                        <ButtonEdit onRefreshUsers={this.onRefreshUsers} user_id={user.id} />
                    </td>
                </tr>
            )
        });

        return (
            <table className="table table-striped table-hover" aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Логин</th>
                        <th>Email</th>
                        <th>Телефон</th>
                        <th>Операции</th>
                    </tr>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </table>
        );
    }

    async componentDidMount() {
        this.GetUsers();
    }

    onRefreshUsers = () => {
        this.GetUsers();
    }

    onButtonEditClickHandler = (e) => {
        e.preventDefault();

        this.setState({ isModalOpen: true });
    }

    GetUsers = async () => {
        const { PLANNER_API_URL } = window;
        const access_token = localStorage.getItem("access_token");

        if (!access_token) return;

        try {
            const url = new URL("users/getall", PLANNER_API_URL);

            const users = await fetchJson(url, {
                method: "GET",
                headers: {
                    "Content-Type": "text/plain",
                    "Authorization": "Bearer " + access_token
                }
            });

            this.setState({ users: users, loading: false });
        }
        catch (err) {
            if (err.response?.status === 401) {
                this.props.onUserLogout();
            }
        }
    }
}
