import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';
import fetchJson from '../../../../utils/fetch-json';

export class ModalEditUser extends Component {
    static displayName = ModalEditUser.name;

    constructor (props) {
        super(props);

        this.state = {
            user: { id: props.user_id },
            listRoles: [],
            listUserGroups: []
        };

        this.state.isAdd = this.state.user.id === 0 || !this.state.user.id;

        this.onInputChangeHandler = this.onInputChangeHandler.bind(this);
        this.onSaveClickHandler = this.onSaveClickHandler.bind(this);
    }

    render() {
        const optionsRoles = this.state.listRoles.map(item => {
            return (<option key={item.id} value={item.id}>{item.name}</option>);
        });

        const optionsUserGroups = this.state.listUserGroups.map(item => {
            return (<option key={item.id} value={item.id}>{item.name}</option>);
        });

        return (
            <Modal isOpen={true} toggle={this.onToggleHandler}>
                <ModalHeader>
                    {this.state.isAdd ? "Новый пользователь" : "Редактирование пользователя"}
                </ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup>
                            <Label for="title">
                                Логин
                            </Label>
                            <Input id="login" name="login" type="text" value={this.state.user.login} onChange={this.onInputChangeHandler} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="description">
                                Email
                            </Label>
                            <Input id="email" name="email" type="text" value={this.state.user.email} onChange={this.onInputChangeHandler} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="phone">
                                Телефон
                            </Label>
                            <Input id="phone" name="phone" type="text" value={this.state.user.phone} onChange={this.onInputChangeHandler} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="telegram_client_id">
                                TelegramId
                            </Label>
                            <Input id="telegram_client_id" name="telegram_client_id" type="text" value={this.state.user.telegram_client_id} onChange={this.onInputChangeHandler} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="role">
                                Роль
                            </Label>
                            <Input id="role" name="role_id" type="select" onChange={this.onInputChangeHandler} value={this.state.user.role_id}>
                                {optionsRoles}
                            </Input>
                        </FormGroup>
                        <FormGroup>
                            <Label for="user_group">
                                Группа
                            </Label>
                            <Input id="user_group" name="user_group_id" type="select" onChange={this.onInputChangeHandler} value={this.state.user.user_group_id}>
                                {optionsUserGroups}
                            </Input>
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.onSaveClickHandler}>
                        {this.state.isAdd ? "Создать" : "Сохранить"}
                    </Button>
                    {' '}
                    <Button onClick={this.onToggleHandler}>
                        Отмена
                    </Button>
                </ModalFooter>
            </Modal>
        );
    }

    onToggleHandler = (e) => {
        e.preventDefault();

        this.props.onModalClosed();
    }

    onInputChangeHandler = (e) => {
        const { name, value } = e.currentTarget;

        let user = this.state.user;
        user[name] = value;

        this.setState({ user: user });
    }

    onSaveClickHandler = async (e) => {
        const { PLANNER_API_URL } = window;
        const access_token = localStorage.getItem("access_token");

        if (!access_token) return;

        try {
            if (this.state.isAdd) {
                let url = new URL("users", PLANNER_API_URL);

                const newUser = await fetchJson(url, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + access_token
                    },
                    body: JSON.stringify(this.state.user)
                });
            }
            else {
                let url = new URL(`users/{this.state.user.id}`, PLANNER_API_URL);

                const newUser = await fetchJson(url, {
                    method: "PUT",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + access_token
                    },
                    body: JSON.stringify(this.state.user)
                });
            }

            this.props.onModalClosed();
            this.props.onRefreshUsers();
        }
        catch (err) {
            if (err.response.status === 401) {
                this.props.onUserLogout();
            }
        }
    }

    async componentDidMount() {
        const { PLANNER_API_URL } = window;
        const access_token = localStorage.getItem("access_token");

        if (!access_token) return;

        try {
            let url = new URL("roles/getall", PLANNER_API_URL);

            const promiseRoles = fetchJson(url, {
                method: "GET",
                headers: {
                    "Content-Type": "text/plain",
                    "Authorization": "Bearer " + access_token
                }
            });

            url = new URL("usergroups/getall", PLANNER_API_URL);

            const promiseUserGroups = fetchJson(url, {
                method: "GET",
                headers: {
                    "Content-Type": "text/plain",
                    "Authorization": "Bearer " + access_token
                }
            });

            const [listRoles, listUserGroups] = await Promise.all([promiseRoles, promiseUserGroups]);

            let { user } = this.state;
            if (this.state.isAdd) {
                user.role_id = listRoles[0].id;
                user.user_group_id = listUserGroups[0].id;
            }
            else {
                url = new URL("users/" + user.id, PLANNER_API_URL);

                const userEdit = await fetchJson(url, {
                    method: "GET",
                    headers: {
                        "Content-Type": "text/plain",
                        "Authorization": "Bearer " + access_token
                    }
                });

                user = userEdit;
            }

            this.setState({ user: user, listRoles: listRoles, listUserGroups: listUserGroups });
        }
        catch (err) {
            if (err.response?.status === 401) {
                this.props.onUserLogout();
            }
        }
    }
}
