import React, { Component } from 'react';
import { Route } from 'react-router';
import { NavMenu } from './components/NavMenu';
import { Login } from './components/Login';
import { Redirect } from 'react-router-dom';
import { Container } from 'reactstrap';

import './custom.css'
import { PlannerTasks } from './components/PlannerTasks';
import { Users } from './components/Admin/Users';

export default class App extends Component {
    static displayName = App.name;

    constructor(props) {
        super(props);

        this.state = {
            isLogin: localStorage.getItem("access_token"),
            isAdmin: localStorage.getItem("is_admin") === "true"
        };

        this.onUserLogin = this.onUserLogin.bind(this);
        this.onUserLogout = this.onUserLogout.bind(this);
    }

    onUserLogin = () => {
        this.setState({ isLogin: true, isAdmin: localStorage.getItem("is_admin") === "true" });
    }

    onUserLogout = () => {
        localStorage.removeItem("access_token");
        localStorage.removeItem("is_admin");
        this.setState({ isLogin: false });
    }
   
    render() {
        return (
            <div>
                <NavMenu isLogin={this.state.isLogin} isAdmin={this.state.isAdmin} onUserLogout={this.onUserLogout} />
                <Container>
                    <Route exact path='/'>
                        <PlannerTasks onUserLogout={this.onUserLogout} />
                    </Route>
                    <Route path='/admin_users'>
                        <Users onUserLogout={this.onUserLogout} />
                    </Route>
                    <Route path='/login'>
                        <Login onUserLogin={this.onUserLogin} />
                    </Route>
                </Container>

                {!this.state.isLogin ? <Redirect to="/login" /> : <Redirect to="/" />}
            </div>
        );
    }
}
